import greenfoot.GreenfootImage;

public class Lever extends Environment implements UsableObject {

    private boolean on;

    public Lever(boolean on) {
        this.on = on;
        toggleImage();
    }

    public void toggle() {
        this.on = !this.on;
        toggleImage();
    }

    public void toggleImage() {
        setImage(new GreenfootImage(String.format("images/usable/lever_%s.png", this.on ? "on" : "off")));
    }

    @Override
    public void use(Player player) {
        System.out.println("lever use");
        toggle();
    }
}
