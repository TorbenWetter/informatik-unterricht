public interface PickableObject {

    default void pick() {
        System.out.println("PickableObject aufgehoben");
    }
}