public class Carrot extends Environment implements PickableObject {

    /**
     * AB Aufgaben2 AttrKonstr
     * Aufgabe 1:
     * a) int weight = 5
     * b)
     * a. ein Konstruktor hat keine Attribute, einer benötigt einen int (der ohne ruft den mit mit Standardwert auf)
     * b. unterschiedliche Anzahl und Typen von Attributen
     */
    private int weight;

    public Carrot() {
        this(5);
    }

    public Carrot(int weight) {
        this.weight = weight;
        print(String.valueOf(weight));
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public boolean isBlocking() {
        return false;
    }
}