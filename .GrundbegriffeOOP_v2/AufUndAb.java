import java.util.*;

public class AufUndAb {

    public static void main(String[] args) {
        int[][] ladders = new int[][]{
                {6, 27}, {14, 19}, {21, 53}, {31, 42}, {33, 38}, {46, 62},
                {51, 59}, {57, 96}, {65, 85}, {68, 80}, {70, 76}, {92, 98}
        };
        int[] field = new int[100 + 1];
        for (int i = 0; i <= 100; i++) {
            field[i] = i;
        }
        for (int i = 0; i < ladders.length; i++) {
            int[] ladder = ladders[i];
            int index1 = ladder[0], index2 = ladder[1];

            int temp = field[index1];
            field[index1] = field[index2];
            field[index2] = temp;
        }

        //int[] field = new int[]{0, 1, 2, 3, 4, 5, 27, 7, 8, 9, 10, 11, 12, 13, 19, 15, 16, 17, 18, 14, 20, 53, 22, 23, 24, 25, 26, 6, 28, 29, 30, 42, 32, 38, 34, 35, 36, 37, 33, 39, 40, 41, 31, 43, 44, 45, 62, 47, 48, 49, 50, 59, 52, 21, 54, 55, 56, 96, 58, 51, 60, 61, 46, 63, 64, 85, 66, 67, 80, 69, 76, 71, 72, 73, 74, 75, 70, 77, 78, 79, 68, 81, 82, 83, 84, 65, 86, 87, 88, 89, 90, 91, 98, 93, 94, 95, 57, 97, 92, 99, 100};

        int endField = 100;
        for (int steps = 1; steps <= 6; steps++) {
            int position = 0;
            List<Integer> visitedFields = new LinkedList<>();
            while (!visitedFields.contains(position) && position != endField) {
                visitedFields.add(position);

                int index = position + steps;
                if (index > endField) {
                    index = 2 * endField - index;
                }
                position = field[index];
            }
            System.out.println("Feld " + position + " wurde nach " + visitedFields.size() + " Schritten erreicht.");
        }
    }
}