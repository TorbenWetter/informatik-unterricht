import greenfoot.Actor;
import greenfoot.GreenfootImage;
import helper.Direction;

import java.awt.*;

/**
 * @author SAE
 */
public class Environment extends Actor {

    private GreenfootImage defaultImage;

    /**
     * Default Contstructor
     */
    public Environment() {
        defaultImage = new GreenfootImage(getImage());
    }

    /**
     * Determines if this part of the environment is blocking.
     *
     * @return true if it is blocking
     */
    public boolean isBlocking() {
        return false;
    }

    public Direction getDirection() {
        return Direction.fromValue(getRotation());
    }

    @Override
    public void turn(int amount) {
        if (amount % 90 != 0) {
            return;
        }
        super.turn(amount);
    }

    @Override
    public void setRotation(int rotation) {
        if (rotation % 90 != 0) {
            return;
        }

        super.setRotation(rotation);
    }


    /**
     * Turns the Actor so he faces upwards
     */
    public void turnUp() {
        turn(Direction.UP);
    }


    /**
     * Turns the Actor so he faces downwards
     */
    public void turnDown() {
        turn(Direction.DOWN);
    }

    /**
     * Turns the Actor so he faces left
     */
    public void turnLeft() {
        turn(Direction.LEFT);
    }

    /**
     * Turns the Actor so he faces right
     */
    public void turnRight() {
        turn(Direction.RIGHT);
    }

    /**
     * Turns toward the given direction.
     *
     * @param direction
     */
    public void turn(Direction direction) {
        int toRotate = direction.getValue() - getRotation();

        if (toRotate != 0) {
            turn(toRotate);
            getImage().rotate(-toRotate);
        }
    }

    @Override
    public void setImage(String fileName) {
        this.setImage(new GreenfootImage(fileName));
    }


    @Override
    public void setImage(GreenfootImage image) {
        Direction currentDirection = Direction.fromValue(getRotation());
        turn(Direction.RIGHT);
        super.setImage(image);
        turn(currentDirection);
    }

    public void resetImage() {
        setImage(defaultImage);
    }

    @Override
    public String toString() {
        return this.getClass().getCanonicalName() + "; at=(" + getX() + ", " + getY() + ")";
    }

    /**
     * Prints the text
     * no text argument resets the text
     *
     * @param text
     */
    public void print(String... text) {
        Direction currentDirection = Direction.fromValue(getRotation());
        turn(Direction.RIGHT);

        for (int i = 0; i < text.length; i++) {
            getImage().drawString(text[i], 0, 20 + (10 * i));
        }

        turn(currentDirection);
    }

    public void setLifeBar(float percent) {
        Direction currentDirection = Direction.fromValue(getRotation());
        turn(Direction.RIGHT);

        getImage().setColor(Color.RED);
        getImage().fillRect(0, 0, (int) (getWorld().getCellSize() * (percent / 100)), getWorld().getCellSize() / 10);

        turn(currentDirection);
    }
}
