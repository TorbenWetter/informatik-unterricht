import greenfoot.Actor;
import greenfoot.Greenfoot;
import greenfoot.World;
import helper.Direction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class Player extends MovingActor {

    //Attribute
    private int life;
    private final int MAX_LIFE = 100;

    private Actor[] inventory;
    private int selectedSlot;
    private InventoryDisplayAdapter adapter;

    //Konstruktoren
    public Player() {
        this(100);
    }

    public Player(int life) {
        this.life = life;
    }

    //Methoden

    /**
     * This method is called by the Greenfoot system when this actor has been inserted into the world.
     *
     * @param world
     */
    public void addedToWorld(World world) {
        inventory = new Actor[Math.min(9, world.getWidth())];
        selectedSlot = 0;
        adapter = new InventoryDisplayAdapter(inventory.length);
        world.addObject(adapter, 0, world.getHeight() - 1);
        adapter.setLinkedItems(inventory);
    }

    /**
     * Wird einmal pro Zeiteinheit aufgerufen
     */
    public void act() {
        performMovement();

        if (this.life <= 0) { // PlayerRabbit is dead..
            getWorld().removeObject(this);
            return;
        }

        resetImage();
        setLifeBar(((float) this.life) / MAX_LIFE * 100);

        adapter.setSelectedSlot(selectedSlot);
    }

    /**
     * AB Aufgaben1 KlassenObjekte
     * Aufgabe 1:
     * a)
     * String getKey()
     * boolean isKeyDown(String keyName)
     * b)
     * getKey() gibt die zuletzt gedrückte Taste als String zurück (null, wenn keine gedrückt wurde seit letztem Aufruf)
     * isKeyDown() gibt einen boolean zurück, ob eine Taste als String aktuell gedrückt ist
     * c)
     * Da die act()-Methode mehrmals pro Sekunde ausgeführt wird und bei getKey() die Taste nach Aufruf zurückgesetzt wird,
     * müsste man Tasten mindestens so oft drücken, wie man die Methode aufruft.
     * Die isKeyDown()-Methode kann problemlos mehrmals pro Sekunde (je Frame) aufgerufen werden
     * d)
     *
     * @see #performMovement()
     * <p>
     * <p>
     * AB Aufgaben3
     * Aufgabe 1:
     * Sie speichert Actors des Typs Carrot, die sich in der Welt auf der angegebenen Position befinden, in eine Liste
     **/

    private void performMovement() {
        HashMap<String, Direction> rotations = new HashMap<String, Direction>() {{
            put("W", Direction.UP);
            put("A", Direction.LEFT);
            put("S", Direction.DOWN);
            put("D", Direction.RIGHT);
        }};
        Iterator it = rotations.entrySet().iterator();
        while (it.hasNext()) {
            HashMap.Entry pair = (HashMap.Entry) it.next();

            if (Greenfoot.isKeyDown((String) pair.getKey())) {
                turn((Direction) pair.getValue());
                move();
                if (getX() == getWorld().getWidth() - 1 && getWorld().getClass() == Level1.class) {
                    Greenfoot.setWorld(new Level2(this));
                }
            }

            it.remove();
        }

        for (int slot = 0; slot < inventory.length; slot++) {
            if (Greenfoot.isKeyDown(String.valueOf(slot + 1))) {
                selectedSlot = slot;
                break;
            }
        }

        if (Greenfoot.isKeyDown("Q")) {
            if (pickUpItem(true) == null) {
                System.out.println("Couldn't pick up any item."); // TODO: print on screen
            }
        }

        if (Greenfoot.isKeyDown("E")) {
            if (!putItem()) {
                System.out.println("Couldn't put any item."); // TODO: print on screen
            }
        }

        if (Greenfoot.isKeyDown("U")) {
            useItem();
        }
    }

    public boolean putItem() {
        Actor item = inventory[selectedSlot];
        if (item == null) {
            return false;
        }

        getWorld().addObject(item, getX(), getY());
        inventory[selectedSlot] = null;
        return true;
    }

    public Actor pickUpItem(boolean addToInventory) {
        if (addToInventory && inventory[selectedSlot] != null) {
            return null;
        }

        ArrayList<Actor> items = (ArrayList<Actor>) getWorld().getObjectsAt(getX(), getY(), Actor.class);
        items.removeIf(item -> !(item instanceof PickableObject));
        if (items.size() > 0) {
            Actor item = items.get(0);
            if (addToInventory) {
                inventory[selectedSlot] = item;
            }
            ((PickableObject) item).pick();
            getWorld().removeObject(item);
            return item;
        }
        return null;
    }

    public void useItem() {
        ArrayList<Actor> items = (ArrayList<Actor>) getWorld().getObjectsAt(getX(), getY(), Actor.class);
        items.removeIf(item -> !(item instanceof UsableObject));
        if (items.size() > 0) {
            Actor item = items.get(0);
            ((UsableObject) item).use(this);
        }
    }

    public void addLife(int life) {
        this.life += life;
        if (this.life > this.MAX_LIFE) {
            this.life = this.MAX_LIFE;
        }
    }

    /**
     * moves one step forward
     */
    public void move() {
        ArrayList<Rock> rocksInFront = (ArrayList<Rock>) getObjectsInFront(Rock.class);
        if (rocksInFront.size() > 0) {
            rocksInFront.get(0).hit();
            this.life -= 10;
            return;
        }

        if (getY() == getWorld().getHeight() - 2 && getDirection() == Direction.DOWN) {
            return;
        }

        super.move(1);
    }
}