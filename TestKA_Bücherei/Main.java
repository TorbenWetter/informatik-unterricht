import java.util.HashSet;
import java.util.Set;

public class Main {

    public static void main(String[] args) {
        Book designPatterns = new Book("Design Patterns", "Torben Wetter", Field.Informatik);
        Book neuronalNetworks = new Book("Neuronal Networks", "Mr. Computer Pro", Field.Informatik);
        Book theWorldIsNotFlat = new Book("The World Is Not Flat", "Angela Merkel", Field.Erdkunde);

        Shelf shelf1 = new Shelf();
        shelf1.addBook(neuronalNetworks);
        shelf1.addBook(theWorldIsNotFlat);

        Shelf shelf2 = new Shelf();
        shelf2.addBook(designPatterns);

        System.out.println("Ansprechpartner für die Bücher im ersten Regal");
        Set<String> contacts = new HashSet<>();
        for (Book book : shelf1.getBooks()) {
            if (book != null) {
                contacts.add(book.getField().getContact());
            }
        }
        System.out.println(String.join(", ", contacts));
    }
}
