public enum Field {

    Informatik("Herr Schaede"), Mathematik("Herr Sahliger"), Erdkunde("Frau Witzleb");

    private String contact;

    Field(String contact) {
        this.contact = contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getContact() {
        return contact;
    }
}
