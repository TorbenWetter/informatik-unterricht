public class Shelf {

    private Book[] books;

    public Shelf() {
        books = new Book[30];
    }

    public Book[] getBooks() {
        // returns a new array without Books that are null (not needed if you check for null in other classes)
        // return Arrays.stream(books).filter(Objects::nonNull).toArray(Book[]::new);
        return books;
    }

    public void addBook(Book book) {
        for (int i = 0; i < books.length; i++) {
            if (books[i] == null) {
                books[i] = book;
                return;
            }
        }
    }

    public void removeBook(Book book) {
        for (int i = 0; i < books.length; i++) {
            if (books[i] == book) {
                books[i] = null;
                return;
            }
        }
    }
}
