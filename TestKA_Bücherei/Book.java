public class Book {

    private String title;
    private String author;
    private Field field;

    public Book(String title, String author, Field field) {
        this.title = title;
        this.author = author;
        this.field = field;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public Field getField() {
        return field;
    }
}
