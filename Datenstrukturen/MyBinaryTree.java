public class MyBinaryTree<T extends Comparable<T>> {

    public class Node implements BinaryTreeNode<T> {

        private BinaryTreeNode<T> parent, leftChild, rightChild;
        private T data;
        private boolean discovered = false; // for depth-first search

        Node(T data) {
            this.data = data;
        }

        @Override
        public void setParent(BinaryTreeNode<T> parent) {
            this.parent = parent;
        }

        @Override
        public void setLeftChild(BinaryTreeNode<T> leftChild) {
            this.leftChild = leftChild;
            if (leftChild != null) {
                leftChild.setParent(this);
            }
        }

        @Override
        public void setRightChild(BinaryTreeNode<T> rightChild) {
            this.rightChild = rightChild;
            if (rightChild != null) {
                rightChild.setParent(this);
            }
        }

        public void setDiscovered(boolean discovered) {
            this.discovered = discovered;
        }

        @Override
        public BinaryTreeNode<T> getParent() {
            return parent;
        }

        @Override
        public BinaryTreeNode<T> getLeftChild() {
            return leftChild;
        }

        @Override
        public BinaryTreeNode<T> getRightChild() {
            return rightChild;
        }

        @Override
        public T getData() {
            return data;
        }

        @Override
        public boolean isDiscovered() {
            return discovered;
        }

        @Override
        public boolean hasParent() {
            return parent != null;
        }

        @Override
        public boolean hasLeftChild() {
            return leftChild != null;
        }

        @Override
        public boolean hasRightChild() {
            return rightChild != null;
        }

        @Override
        public boolean isLeftChild(BinaryTreeNode<T> node) {
            return leftChild == node;
        }

        @Override
        public boolean isRightChild(BinaryTreeNode<T> node) {
            return rightChild == node;
        }

        @Override
        public void resetDiscovery() {
            discovered = false;
            if (leftChild != null) {
                leftChild.resetDiscovery();
            }
            if (rightChild != null) {
                rightChild.resetDiscovery();
            }
        }
    }

    private BinaryTreeNode<T> root;

    public BinaryTreeNode<T> getRoot() {
        return root;
    }

    public void setRoot(BinaryTreeNode<T> root) {
        this.root = root;
        root.setParent(null);
    }

    public void insert(T data) {
        if (root == null) {
            root = new Node(data);
        } else {
            insert(data, root);
        }
    }

    private void insert(T data, BinaryTreeNode<T> parent) {
        if (data.compareTo(parent.getData()) < 0) { // insert left
            if (parent.getLeftChild() == null) {
                parent.setLeftChild(new Node(data));
            } else {
                insert(data, parent.getLeftChild());
            }
        } else { // insert right
            if (parent.getRightChild() == null) {
                parent.setRightChild(new Node(data));
            } else {
                insert(data, parent.getRightChild());
            }
        }
    }

    // breadth-first search; returns the amount of passed edges (-1 if not found)
    public int bfs(T searchedData) {
        int count = 0;
        MyQueue<BinaryTreeNode<T>> queue = new MyQueue<>();
        queue.enqueue(root);
        while (!queue.isEmpty()) {
            BinaryTreeNode<T> current = queue.dequeue();
            if (current.getData() == searchedData) {
                return count;
            }

            if (current.getLeftChild() != null) {
                queue.enqueue(current.getLeftChild());
            }
            if (current.getRightChild() != null) {
                queue.enqueue(current.getRightChild());
            }
            count++;
        }
        return -1;
    }

    // depth-first search; returns whether the data was found
    public boolean contains(T data) {
        MyStack<BinaryTreeNode<T>> stack = new MyStack<>();
        stack.push(root);
        while (!stack.isEmpty()) {
            BinaryTreeNode<T> current = stack.pop();
            if (!current.isDiscovered()) {
                if (current.getData() == data) {
                    root.resetDiscovery();
                    return true;
                }
                current.setDiscovered(true);
                if (current.getLeftChild() != null) {
                    stack.push(current.getLeftChild());
                }
                if (current.getRightChild() != null) {
                    stack.push(current.getRightChild());
                }
            }
        }
        root.resetDiscovery();
        return false;
    }

    private String inOrder(BinaryTreeNode<T> parent) {
        if (parent == null) {
            return "";
        }

        BinaryTreeNode<T> left = parent.getLeftChild(), right = parent.getRightChild();
        return inOrder(left) + parent.getData() + " " + inOrder(right);
    }

    // depth-first "search"; returns all data in inorder
    public String dft() {
        return inOrder(root);
    }

    @Override
    public String toString() {
        return BinaryTreePrinter.print(root);
    }
}
