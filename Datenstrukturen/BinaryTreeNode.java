public interface BinaryTreeNode<T> {

    void resetDiscovery();

    void setParent(BinaryTreeNode<T> parent);

    void setLeftChild(BinaryTreeNode<T> leftChild);

    void setRightChild(BinaryTreeNode<T> rightChild);

    void setDiscovered(boolean discovered);

    BinaryTreeNode<T> getParent();

    BinaryTreeNode<T> getLeftChild();

    BinaryTreeNode<T> getRightChild();

    T getData();

    boolean isDiscovered();

    boolean isLeftChild(BinaryTreeNode<T> node);

    boolean isRightChild(BinaryTreeNode<T> node);

    boolean hasParent();

    boolean hasLeftChild();

    boolean hasRightChild();

}