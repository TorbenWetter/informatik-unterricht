public class MyEdge implements Comparable<MyEdge> {

    private MyVertex from;
    private MyVertex to;
    private float weight;

    public MyEdge(MyVertex from, MyVertex to, float weight) {
        this.from = from;
        this.to = to;
        this.weight = weight;
        from.getEdges().add(this);
    }

    public MyVertex getFrom() {
        return from;
    }

    public MyVertex getTo() {
        return to;
    }

    public float getWeight() {
        return weight;
    }

    @Override
    public int compareTo(MyEdge o) {
        return Float.compare(weight, o.getWeight());
    }

    @Override
    public String toString() {
        return from + "-" + weight + "->" + to;
    }
}
