import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DotGraphTool {

    public static String graphToDotString(MyGraph graph) {
        StringBuilder result = new StringBuilder();

        result.append("Digraph {\n");

        for (MyVertex vertex : graph.getVertices()) {
            result.append("  ").append(vertex.getName()).append(" [label=\"").append(vertex.getName()).append("\"];\n");
        }

        for (MyEdge edge : graph.getEdges()) {
            result.append("  ").append(edge.getFrom().getName()).append(" -> ").append(edge.getTo().getName()).append(" [label=\"").append(edge.getWeight()).append("\"];\n");
        }
        result.append("}");

        return result.toString();
    }

    public static MyGraph dotStringToGraph(String dotString, boolean directed) {
        dotString = dotString.substring(dotString.indexOf("{") + 1, dotString.indexOf("}"));
        dotString = dotString.replace("\n", "");
        dotString = dotString.replace(" ", "");
        System.out.println(dotString);

        MyGraph graph = new MyGraph(directed);

        Pattern vertexPattern = Pattern.compile("([a-zA-Z]+[0-9]*)\\[label=\"([a-zA-Z]+[0-9]*)\"");
        Matcher vertexMatcher = vertexPattern.matcher(dotString);

        while (vertexMatcher.find()) {
            String name = vertexMatcher.group(1);
            MyVertex vertex = new MyVertex(name);
            graph.getVertices().add(vertex);
        }

        Pattern edgePattern = Pattern.compile("([a-zA-Z]+[0-9]*)->([a-zA-Z]+[0-9]*)\\[label=\"([0-9]+(\\.[0-9]+)?)\"");
        Matcher edgeMatcher = edgePattern.matcher(dotString);

        while (edgeMatcher.find()) {
            MyVertex from = getOrCreateVertex(graph, edgeMatcher.group(1));
            MyVertex to = getOrCreateVertex(graph, edgeMatcher.group(2));
            float weight = Float.valueOf(edgeMatcher.group(3));

            MyEdge edge = new MyEdge(from, to, weight);

            graph.getEdges().add(edge);
        }

        return graph;
    }

    private static MyVertex getOrCreateVertex(MyGraph graph, String name) {
        for (MyVertex vertex : graph.getVertices()) {
            if (vertex.getName().equals(name)) {
                return vertex;
            }
        }
        MyVertex vertex = new MyVertex(name);
        graph.getVertices().add(vertex);
        return vertex;
    }
}