import java.util.*;

public class MyGraph {

    private List<MyVertex> vertices;
    private boolean directed;

    public MyGraph(boolean directed) {
        this.vertices = new LinkedList<>();
        this.directed = directed;
    }

    public void addVertex(MyVertex vertex) {
        vertices.add(vertex);
    }

    public void removeVertex(MyVertex vertex) {
        vertices.remove(vertex);
    }

    public List<MyVertex> getVertices() {
        return vertices;
    }

    public MyVertex getVertexByName(String name) {
        for (MyVertex vertex : vertices) {
            if (vertex.getName().equals(name)) {
                return vertex;
            }
        }
        return null;
    }

    public List<MyEdge> getEdges() {
        return new LinkedList<MyEdge>() {{
            for (MyVertex vertex : vertices) {
                addAll(vertex.getEdges());
            }
        }};
    }

    public boolean containsVertex(String name) {
        for (MyVertex vertex : vertices) {
            if (vertex.getName().equals(name)) {
                return true;
            }
        }
        return false;
    }

    public boolean isDirected() {
        return directed;
    }

    public boolean hasConnectionBetween(MyVertex v1, MyVertex v2) {
        List<MyVertex> visited = new LinkedList<>();
        visited.add(v1);

        Queue<MyVertex> queue = new LinkedList<>();
        queue.add(v1);

        while (!queue.isEmpty()) {
            MyVertex vertex = queue.poll();
            if (vertex == v2) {
                return true;
            }

            for (MyVertex neighbor : vertex.getNeighbors()) {
                if (!visited.contains(neighbor)) {
                    visited.add(neighbor);
                    queue.add(neighbor);
                }
            }
        }
        return false;
    }

    // Kruskal's algorithm
    public MyGraph getKruskalSpanningTree() {
        MyGraph spanningTree = new MyGraph(directed);
        for (MyVertex graphVertex : vertices) {
            // clone vertex (without list of edges)
            spanningTree.addVertex(new MyVertex(graphVertex.getName()));
        }

        // sort edges by weight
        List<MyEdge> graphEdges = getEdges();
        Collections.sort(graphEdges);

        // for every Edge in graph
        for (MyEdge graphEdge : graphEdges) {
            // if there's no connection between the two Vertices in the Spanning Tree already
            MyVertex fromVertex = spanningTree.getVertexByName(graphEdge.getFrom().getName());
            MyVertex toVertex = spanningTree.getVertexByName(graphEdge.getTo().getName());
            if (!spanningTree.hasConnectionBetween(fromVertex, toVertex)) {
                // add Edge to Spanning Tree
                new MyEdge(fromVertex, toVertex, graphEdge.getWeight());
                if (!directed) {
                    new MyEdge(toVertex, fromVertex, graphEdge.getWeight());
                }
            }
        }
        return spanningTree;
    }

    // Prim's algorithm
    public MyGraph getPrimSpanningTree() {
        MyGraph spanningTree = new MyGraph(directed);
        if (vertices.size() < 1) {
            return spanningTree;
        }

        MyVertex startVertex = new MyVertex(vertices.get(0).getName());
        spanningTree.addVertex(startVertex);

        // stores edges from visited vertices to non-visited vertices
        PriorityQueue<MyEdge> edges = new PriorityQueue<>(vertices.get(0).getEdges());
        while (edges.size() > 0) {
            // find minimum edge
            MyEdge minimalEdge = edges.poll();

            // get fromVertex (which has already been added to spanningTree)
            MyVertex fromVertex = spanningTree.getVertexByName(minimalEdge.getFrom().getName());

            // create new toVertex (with same name) and add to spanningTree
            MyVertex toVertex = new MyVertex(minimalEdge.getTo().getName());
            spanningTree.addVertex(toVertex);

            // add Edge to Spanning Tree
            new MyEdge(fromVertex, toVertex, minimalEdge.getWeight());
            if (!directed) {
                new MyEdge(toVertex, fromVertex, minimalEdge.getWeight());
            }

            // remove invalid edges (which lead to an already visited vertex)
            edges.removeIf(edge -> spanningTree.containsVertex(edge.getTo().getName()));

            // add new valid edges
            for (MyEdge edge : getVertexByName(toVertex.getName()).getEdges()) {
                if (!spanningTree.containsVertex(edge.getTo().getName())) {
                    edges.add(edge);
                }
            }
        }
        return spanningTree;
    }
}
