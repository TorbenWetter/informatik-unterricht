import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class MyVertex {

    private String name;
    private List<MyEdge> edges;

    public MyVertex(String name) {
        this.name = name;
        this.edges = new LinkedList<>();
    }

    public String getName() {
        return name;
    }

    public List<MyEdge> getEdges() {
        return edges;
    }

    public MyEdge getEdge(MyVertex to) {
        for (MyEdge edge : edges) {
            if (edge.getTo() == to) {
                return edge;
            }
        }
        return null;
    }

    public List<MyVertex> getNeighbors() {
        return edges.stream().map(MyEdge::getTo).collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return name;
    }
}
