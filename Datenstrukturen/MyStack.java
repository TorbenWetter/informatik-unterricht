public class MyStack<T> {

    private class Element {
        T data;
        Element next;

        Element(T data, Element next) {
            this.data = data;
            this.next = next;
        }
    }

    private Element top;

    public boolean isEmpty() {
        return top == null;
    }

    public void push(T data) {
        top = new Element(data, top);
    }

    public T pop() {
        if (top == null) {
            return null;
        }

        T data = top.data;
        top = top.next;
        return data;
    }

    public T peek() {
        return top.data;
    }
}
