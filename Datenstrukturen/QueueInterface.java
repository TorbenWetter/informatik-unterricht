public interface QueueInterface<T> {
    public boolean isEmpty();
    public void enqueue(T object);
    public T front();
    public T dequeue();
}
