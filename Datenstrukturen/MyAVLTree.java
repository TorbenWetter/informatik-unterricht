import java.util.*;

public class MyAVLTree<T extends Comparable<T>> extends MyBinaryTree<T> {

    private class AVLNode extends Node {

        private int height;

        private int balance;

        AVLNode(T data) {
            super(data);
            height = 1;
        }

        public int getHeight() {
            return height;
        }

        public void setHeight(int height) {
            this.height = height;
        }

        public int getBalance() {
            return balance;
        }

        public void setBalance(int balance) {
            this.balance = balance;
        }
    }

    @Override
    public void insert(T data) {
        if (getRoot() == null) {
            setRoot(new AVLNode(data));
        } else {
            insert(data, (AVLNode) getRoot());
        }
    }

    private void insert(T data, AVLNode node) {
        if (data.compareTo(node.getData()) < 0) { // insert left
            if (node.getLeftChild() == null) {
                node.setLeftChild(new AVLNode(data));
            } else {
                insert(data, (AVLNode) node.getLeftChild());
            }
        } else { // insert right
            if (node.getRightChild() == null) {
                node.setRightChild(new AVLNode(data));
            } else {
                insert(data, (AVLNode) node.getRightChild());
            }
        }

        int upperBalance = calculateBalance(node);

        // AVL "valid" -> no rotation needed
        if (upperBalance == 0 || Math.abs(upperBalance) == 1) {
            return;
        }

        AVLNode leftChild = (AVLNode) node.getLeftChild();
        AVLNode rightChild = (AVLNode) node.getRightChild();

        // test the balance(s) on both sides, left and right
        List<AVLNode> lowerNodes = new LinkedList<>(Arrays.asList(leftChild, rightChild));
        lowerNodes.removeIf(Objects::isNull);
        for (AVLNode lowerNode : lowerNodes) {
            int lowerBalance = lowerNode.getBalance();

            // rotate, depending on the balance of the upper and lower Node
            if (upperBalance >= 2 && lowerBalance == 1) {
                leftRotate(node);
            } else if (upperBalance <= -2 && lowerBalance == -1) {
                rightRotate(node);
            } else if (upperBalance >= 2 && lowerBalance == -1) {
                rightRotate(lowerNode);
                leftRotate(node);
            } else if (upperBalance <= -2 && lowerBalance == 1) {
                leftRotate(lowerNode);
                rightRotate(node);
            }
        }
    }

    public int calculateBalance(AVLNode node) {
        // calculate and set the height
        AVLNode leftChild = (AVLNode) node.getLeftChild();
        AVLNode rightChild = (AVLNode) node.getRightChild();
        int leftSubTreeHeight = leftChild != null ? leftChild.getHeight() : 0;
        int rightSubTreeHeight = rightChild != null ? rightChild.getHeight() : 0;
        node.setHeight(Math.max(leftSubTreeHeight, rightSubTreeHeight) + 1);

        // set the balance
        int upperBalance = rightSubTreeHeight - leftSubTreeHeight;
        node.setBalance(upperBalance);
        return upperBalance;
    }

    public void rightRotate(AVLNode upperNode) {
        AVLNode lowerNode = (AVLNode) upperNode.getLeftChild();
        upperNode.setLeftChild(lowerNode.getRightChild());
        if (upperNode.hasParent()) {
            if (upperNode.getParent().isRightChild(upperNode)) {
                upperNode.getParent().setRightChild(lowerNode);
            } else { // is left child
                upperNode.getParent().setLeftChild(lowerNode);
            }
        } else {
            setRoot(lowerNode);
        }
        lowerNode.setRightChild(upperNode);

        calculateBalance(upperNode);
        calculateBalance(lowerNode);
    }

    public void leftRotate(AVLNode upperNode) {
        AVLNode lowerNode = (AVLNode) upperNode.getRightChild();
        upperNode.setRightChild(lowerNode.getLeftChild());
        if (upperNode.hasParent()) {
            if (upperNode.getParent().isRightChild(upperNode)) {
                upperNode.getParent().setRightChild(lowerNode);
            } else { // is left child
                upperNode.getParent().setLeftChild(lowerNode);
            }
        } else {
            setRoot(lowerNode);
        }
        lowerNode.setLeftChild(upperNode);

        calculateBalance(upperNode);
        calculateBalance(lowerNode);
    }
}
