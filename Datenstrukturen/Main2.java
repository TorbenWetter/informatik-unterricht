import java.util.Arrays;

public class Main2 {

    public static void main(String[] args) {
        /*MyQueue<String> queue = new MyQueue<>();
        queue.enqueue("Torben");
        queue.enqueue("Laura");
        queue.enqueue("Sina");
        queue.enqueue("Johanna");
        queue.enqueue("Linda");
        queue.enqueue("Jonas");
        System.out.println(queue);
        String out = queue.dequeue();
        System.out.println(out + " | " + queue);
        queue.enqueue("Tobias");
        System.out.println(queue);

        System.out.println("---------------");*/

        /*MyAVLTree<Integer> avlTree = new MyAVLTree<>();
        avlTree.insert(5);
        System.out.println(avlTree);
        avlTree.insert(6);
        System.out.println(avlTree);
        avlTree.insert(13);
        System.out.println(avlTree);
        avlTree.insert(12);
        System.out.println(avlTree);
        avlTree.insert(11);
        System.out.println(avlTree);
        avlTree.insert(10);
        System.out.println(avlTree);*/

        /*MyVertex v1 = new MyVertex("v1");
        MyVertex v2 = new MyVertex("v2");
        MyVertex v3 = new MyVertex("v3");
        MyVertex v4 = new MyVertex("v4");
        MyVertex v5 = new MyVertex("v5");
        MyVertex v6 = new MyVertex("v6");

        MyGraph graph = new MyGraph(true);
        for (MyVertex vertex : Arrays.asList(v1, v2, v3, v4, v5, v6)) {
            graph.addVertex(vertex);
        }

        new MyEdge(v1, v2, 3);
        new MyEdge(v1, v3, 2);
        new MyEdge(v1, v5, 6);
        new MyEdge(v2, v3, 3);
        new MyEdge(v2, v5, 1);
        new MyEdge(v3, v4, 4);
        new MyEdge(v3, v6, 5);
        new MyEdge(v4, v6, 2);
        new MyEdge(v5, v3, 3);
        new MyEdge(v5, v4, 1);
        new MyEdge(v5, v6, 5);

        String graphDotString = DotGraphTool.graphToDotString(graph);
        System.out.println(graphDotString);*/

        MyVertex a = new MyVertex("A");
        MyVertex b = new MyVertex("B");
        MyVertex c = new MyVertex("C");
        MyVertex d = new MyVertex("D");
        MyVertex e = new MyVertex("E");
        MyVertex f = new MyVertex("F");
        MyVertex g = new MyVertex("G");

        MyGraph graph2 = new MyGraph(false);
        for (MyVertex vertex : Arrays.asList(a, b, c, d, e, f, g)) {
            graph2.addVertex(vertex);
        }

        new MyEdge(a, b, 7);
        new MyEdge(a, d, 5);
        new MyEdge(b, a, 7);
        new MyEdge(b, c, 8);
        new MyEdge(b, d, 9);
        new MyEdge(b, e, 7);
        new MyEdge(c, b, 8);
        new MyEdge(c, e, 5);
        new MyEdge(d, a, 5);
        new MyEdge(d, b, 9);
        new MyEdge(d, e, 15);
        new MyEdge(d, f, 6);
        new MyEdge(e, b, 7);
        new MyEdge(e, c, 5);
        new MyEdge(e, d, 15);
        new MyEdge(e, f, 8);
        new MyEdge(e, g, 9);
        new MyEdge(f, d, 6);
        new MyEdge(f, e, 8);
        new MyEdge(f, g, 11);
        new MyEdge(g, e, 9);
        new MyEdge(g, f, 11);

        String graph2DotString = DotGraphTool.graphToDotString(graph2);
        System.out.println(graph2DotString);

        MyGraph spanningTree = graph2.getPrimSpanningTree();
        String spanningTreeDotString = DotGraphTool.graphToDotString(spanningTree);
        System.out.println(spanningTreeDotString);
    }
}
