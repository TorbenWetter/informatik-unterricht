public class MyQueue<T> implements QueueInterface<T> {

    private class Element {
        T data;
        Element next;

        Element(T data) {
            this.data = data;
        }
    }

    private Element first, last;

    public T front() {
        if (!isEmpty()) {
            return first.data;
        }
        return null;
    }

    public boolean isEmpty() {
        return first == null;
    }

    public void enqueue(T object) {
        Element newLast = new Element(object);
        if (!isEmpty()) { // not empty
            last.next = newLast;
        } else { // empty
            first = newLast;
        }
        last = newLast;
    }

    public T dequeue() {
        if (!isEmpty()) { // not empty
            T toReturn = this.first.data;
            if (this.first == this.last) { // size == 1
                this.first = null;
                this.last = null;
            } else if (this.first.next == this.last) { // size == 2
                this.first = this.last;
            } else { // size > 2
                this.first = this.first.next;
            }
            return toReturn;
        }
        return null;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        Element current = first;
        do {
            sb.append(current.data.toString()).append(", ");
        } while ((current = current.next) != null);
        return sb.delete(sb.length() - 2, sb.length()).toString();
    }
}
