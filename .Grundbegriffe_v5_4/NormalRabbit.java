/**
 * Ein ganz normaler Hase.
 * <p>
 * Benutze die init() Methode um dem Hasen Anweisungen zu geben, die er beim Programmstart ausf�hrt
 */
public class NormalRabbit extends Rabbit {

    /**
     * Die init() Methode, wird einmalig beim Programmstart aufgerufen.
     * Hier kannst du alle Anweisungen eintragen, die du dem Hasen geben m�chtest.
     */
    public void init() {
        /* Aufgabe 1
        putCarrot();
        movePutN(6);
        for (int i = 0; i < 2; i++) {
            turnRight();
        moveN(3);
        turnLeft();
        movePutN(6);

        --- ohne Vorkenntnisse ---

        putCarrot();
        putMove();
        putMove();
        putMove();
        putMove();
        putMove();
        putMove();
        turnRight();
        turnRight();
        move();
        move();
        move();
        turnLeft();
        putMove();
        putMove();
        putMove();
        putMove();
        putMove();
        putMove();
        */
       
        /* Aufgabe 2
        for (int i = 0; i < 4; i++) {
            movePutN(6);
            turnRight();
        }
        turnRight();
        moveN(3);
        turnLeft();
        movePutN(5);

        --- ohne Vorkenntnisse ---

        putMove();
        putMove();
        putMove();
        putMove();
        putMove();
        putMove();
        turnRight();
        putMove();
        putMove();
        putMove();
        putMove();
        putMove();
        putMove();
        turnRight();
        putMove();
        putMove();
        putMove();
        putMove();
        putMove();
        putMove();
        turnRight();
        putMove();
        putMove();
        putMove();
        putMove();
        putMove();
        putMove();
        turnRight();
        turnRight();
        move();
        move();
        move();
        turnLeft();
        putMove();
        putMove();
        putMove();
        putMove();
        putMove();
        */
    }

    public void moveN(int n) {
        for (int i = 0; i < n; i++)
            move();
    }

    public void putMove() {
        move();
        putCarrot();
    }

    public void movePutN(int n) {
        for (int i = 0; i < n; i++) {
            move();
            putCarrot();
        }
    }

    public void turnRight() {
        for (int i = 0; i < 3; i++)
            turnLeft();
        /*
        --- ohne Vorkenntnisse ---

        turnLeft();
        turnLeft();
        turnLeft();
        */
    }
}
