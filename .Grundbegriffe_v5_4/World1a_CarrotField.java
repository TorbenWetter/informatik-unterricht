import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class World1a_CarrotField here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class World1a_CarrotField extends WorldRabbitWorld
{

    public World1a_CarrotField(){
        super();
    }

	public void populate() {
		
		addObject(new Carrot(), 3, 3);
		addObject(new Carrot(), 4, 3);
		addObject(new Carrot(), 5, 3);
		addObject(new Carrot(), 6, 3);

		addObject(new Carrot(), 3, 4);
		addObject(new Carrot(), 4, 4);
		addObject(new Carrot(), 5, 4);
		addObject(new Carrot(), 6, 4);

		addObject(new Carrot(), 3, 5);
		addObject(new Carrot(), 4, 5);
		addObject(new Carrot(), 5, 5);
		addObject(new Carrot(), 6, 5);

	}
}
