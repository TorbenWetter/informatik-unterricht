public class World3d_GiantCarrotField extends WorldRabbitWorld {


    public World3d_GiantCarrotField() {
        super(25, 25);

        prepare();
    }

    public void prepare() {
        /* Aufgabe 10
        addObject(new CarrotRabbit5(), 24, 24);
        */
    }

    public void populate() {
        for (int i = 0; i < 25; i++) {
            for (int j = 0; j < 25; j++) {
                addObject(new Carrot(), i, j);
            }
        }
        removeObject(getObjectsAt(24, 24, Carrot.class).get(0));
    }

}
