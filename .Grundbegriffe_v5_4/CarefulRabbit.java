
// (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

import greenfoot.Actor;

/**
 * Eine weiterentwickelte Version eines Rabbit, der seine Umgebung wahrnehmen kann.
 *
 * @see isCarrotNear(), canMove(), isFacing...
 */
public abstract class CarefulRabbit extends Rabbit {

    protected static final int UP = 270;
    protected static final int RIGHT = 0;
    protected static final int DOWN = 90;
    protected static final int LEFT = 180;

    /**
     * Erzeuge einen Hasen
     */
    public CarefulRabbit() {
        super();
    }


    /**
     * Erzeuge einen Hasen
     *
     * @param carrotCount Anzahl der Karotten die der Hase von Anfang an bei sich tr�gt.
     */
    public CarefulRabbit(int carrotCount) {
        super(carrotCount);


    }

    /**
     * �berpr�fe, ob der Hase auf einer Karotte steht.
     *
     * @return true wenn der Hase auf einer Karotte steht
     */
    public boolean isCarrotNear() {
        Actor carrot = getOneObjectAtOffset(0, 0, Carrot.class);
        if (carrot != null) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * �berpr�fe, ob der Hase nach vorne laufen kann.
     *
     * @return true wenn der Hase das Feld vor dem Hasen frei ist.
     */
    public boolean canMove() {
        int x = getX();
        int y = getY();
        switch (direction) {
            case DOWN:
                y++;
                break;
            case RIGHT:
                x++;
                break;
            case UP:
                y--;
                break;
            case LEFT:
                x--;
                break;
        }

        return validField(x, y);
    }


    /**
     * �berpr�ft ob der Hase nach Norden schaut
     *
     * @return true wenn der Hase Richtung Norden schaut
     */
    public boolean isFacingNorth() {
        if (direction == UP) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * �berpr�ft ob der Hase nach S�den schaut
     *
     * @return true wenn der Hase Richtung S�den schaut
     */
    public boolean isFacingSouth() {
        if (direction == DOWN) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * �berpr�ft ob der Hase nach Osten schaut
     *
     * @return true wenn der Hase Richtung Osten schaut
     */
    public boolean isFacingEast() {
        if (direction == RIGHT) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * �berpr�ft ob der Hase nach Westen schaut
     *
     * @return true wenn der Hase Richtung Westen schaut
     */
    public boolean isFacingWest() {
        if (direction == LEFT) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isInFront(Class object) {
        int xOffset = 0, yOffset = 0;
        if (isFacingEast())
            xOffset = 1;
        else if (isFacingSouth())
            yOffset = 1;
        else if (isFacingWest())
            xOffset = -1;
        else if (isFacingNorth())
            yOffset = -1;
        int newX = getX() + xOffset, newY = getY() + yOffset;
        return getWorld().getObjectsAt(newX > -1 ? newX : 0, newY > -1 ? newY : 0, object).size() != 0;
    }
}
