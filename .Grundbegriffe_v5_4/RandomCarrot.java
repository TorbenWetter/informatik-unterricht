

/**
 * Write a description of class ZufallsKarotte here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class RandomCarrot extends Carrot
{

    private double propability = 1.0d;
    private boolean initDone = false;
    
    public RandomCarrot(double propability){
        this.propability = propability;       
    }
    
    public void act(){
         if(!initDone){
            init();
            initDone = true;
        }
    }
    
    public void init(){
        if(propability > Math.random()){
            getWorld().removeObject(this);
        }
    }
}
