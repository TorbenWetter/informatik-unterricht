public class World3e_RandomCarrotRow extends WorldRabbitWorld {

    public World3e_RandomCarrotRow() {
        super(25, 25);

        prepare();
    }

    public void prepare() {
        /* Aufgabe 11
        addObject(new CarrotRabbit6(), 0, 0);
        */
    }

    public void populate() {
        for (int i = 0; i < 25; i++) {
            addObject(new RandomCarrot(0.5), i, 0);
        }
    }
}
