public class CarrotRabbit6 extends CarefulRabbit {

    public void init() {
        /* Aufgabe 11
        carefulPick();
        while (canMove()) {
            move();
            carefulPick();
        }
        */
    }

    private void carefulPick() {
        if (isCarrotNear()) {
            pickCarrot();
        }
    }
}
