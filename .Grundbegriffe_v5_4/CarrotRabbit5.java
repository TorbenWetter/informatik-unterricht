import java.awt.*;

public class CarrotRabbit5 extends CarefulRabbit {

    public void init() {
        /* Aufgabe 10
        turnLeft();
        Point position;
        do {
            position = new Point(getX(), getY());
            while (canMove() && isInFront(Carrot.class)) {
                move();
                pickCarrot();
            }
            turnLeft();
        } while (!position.equals(new Point(getX(), getY())));

        --- ohne Vorkenntnisse ---

        turnLeft();
        while (isCarrotOnLeft() || isAtEdge() || isCarrotInFront()) {
            while (canMove() && isCarrotInFront()) {
                move();
                pickCarrot();
            }
            turnLeft();
        }
        */
    }

    private boolean isCarrotInFront() {
        if (!canMove()) {
            return false;
        }

        move();
        boolean onCarrot = isCarrotNear();
        turnAround();
        move();
        turnAround();
        return onCarrot;
    }

    private boolean isCarrotOnLeft() {
        turnLeft();
        move();
        boolean onCarrot = isCarrotNear();
        turnAround();
        move();
        turnLeft();
        return onCarrot;
    }

    private void turnAround() {
        turnLeft();
        turnLeft();
    }
}
