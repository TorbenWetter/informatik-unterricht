import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Diese Datei enthält diverse Fehler, finde sie und korrigiere sie.
 * Um welche Typen von Fehlern handelt es sich?
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class ErrorRabbit extends NormalRabbit {
    public void init() {
        quadrat(); // () haben gefehlt
    }

    public void quadrat() { // void hat gefehlt
        putMove();
        putMove();
        turnLeft();
        putMove(); // m war klein
        putMove();
        turnLeft();
        putMove(); // ; hat gefehlt
        putMove();
        // ein putMove() zu viel
        turnLeft(); // turnRight() statt turnLeft()
        putMove();
        putMove(); // ein putMove() zu wenig
        turnLeft();
    }

    public void putMove() { // pubic statt public und Methode in einer Methode..
        putCarrot();
        move();
    }
}
