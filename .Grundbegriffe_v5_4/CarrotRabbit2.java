public class CarrotRabbit2 extends CarefulRabbit {

    public void init() {
        /* Aufgabe 7
        moveN(3);
        for (int i = 0; i < 2; i++) {
            if (!isCarrotNear()) putCarrot();
            move();
        }
        moveN(3);

        --- ohne Vorkenntnisse ---

        move();
        move();
        move();
        if (isCarrotNear() != true) {
            putCarrot();
        }
        move();
        if (isCarrotNear() != true) {
            putCarrot();
        }
        move();
        move();
        move();
        move();
        */
    }

    private void moveN(int n) {
        for (int i = 0; i < n; i++)
            move();
    }
}
