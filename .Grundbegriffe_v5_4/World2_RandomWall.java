/**
 * Write a description of class WorldRandomWall here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class World2_RandomWall extends WorldRabbitWorld {

    public World2_RandomWall() {
        super();

        prepare();
    }

    public void prepare() {
        /* Aufgabe 5
        addObject(new RockRabbit(), 0, 3);
        */
    }

    public void populate() {

        addObject(new Rock(), 3, 0);
        addObject(new Rock(), 3, 1);
        addObject(new Rock(), 3, 2);
        addObject(new RandomRock(0.5), 3, 3);
        addObject(new Rock(), 3, 4);
        addObject(new Rock(), 3, 5);
        addObject(new Rock(), 3, 6);
        addObject(new Rock(), 3, 7);
    }
}
