public class World1_CarrotRow extends WorldRabbitWorld {

    public World1_CarrotRow() {
        super(20, 25);

        prepare();
    }

    private void prepare() {
        /* Aufgabe 4
        addObject(new MoveRabbit(), 0, 0);
        */
        /* Aufgabe Fehlererkennung
        addObject(new ErrorRabbit(), 5, 5);
        */
        /* Aufgabe 8
        addObject(new CarrotRabbit3(), 0, 0);
        */
    }

    public void populate() {
        for (int i = 0; i < 20; i++) {

            addObject(new Carrot(), i, 0);

        }
    }
}
