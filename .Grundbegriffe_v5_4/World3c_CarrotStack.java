public class World3c_CarrotStack extends WorldRabbitWorld {

    public World3c_CarrotStack() {
        super();

        prepare();
    }

    private void prepare() {
        /* Aufgabe 9
        addObject(new CarrotRabbit4(), 2, 2);
        */
        /* Aufgabe 13
        addObject(new CarrotRabbit7(), 2, 2);
        */
        /* Aufgabe 14
        addObject(new CarrotRabbit8(), 2, 2);
        */
    }

    public void populate() {
        for (int i = 0; i < 250; i++) {
            addObject(new RandomCarrot(0.5), 2, 2);
        }
    }
}
