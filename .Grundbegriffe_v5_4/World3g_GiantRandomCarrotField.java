/**
 * Write a description of class GrossesZufallsKarottenFeld here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class World3g_GiantRandomCarrotField extends WorldRabbitWorld
{


    public World3g_GiantRandomCarrotField()	
    {
       super(25,25);
    }
    
    public void populate(){
        for(int i=0;i<25;i++){
            for(int j=0;j<25;j++){
                addObject(new RandomCarrot(0.5),i,j);
            }
        }
        
    }
    
}
