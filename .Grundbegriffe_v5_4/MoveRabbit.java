/**
 * Ein sich bewegender Hase.
 * <p>
 * Benutze die init() Methode um dem Hasen Anweisungen zu geben, die er beim Programmstart ausf�hrt
 */
public class MoveRabbit extends NormalRabbit {

    /**
     * Die init() Methode, wird einmalig beim Programmstart aufgerufen.
     * Hier kannst du alle Anweisungen eintragen, die du dem Hasen geben m�chtest.
     */
    public void init() {
        /* Aufgabe 3
        doubleMove();
        sleep();
        turnAround();
        sleep();
        putMove();
        */

        /* Aufgabe 4
        pickCarrot();
        for (int i = 0; i < 19; i++)
            pickMove();

        --- ohne Vorkenntnisse ---

        pickCarrot();
        pickMove();
        pickMove();
        pickMove();
        pickMove();
        pickMove();
        pickMove();
        pickMove();
        pickMove();
        pickMove();
        pickMove();
        pickMove();
        pickMove();
        pickMove();
        pickMove();
        pickMove();
        pickMove();
        pickMove();
        pickMove();
        pickMove();
        */
    }

    public void pickMove() {
        move();
        pickCarrot();
    }

    public void doubleMove() {
        for (int i = 0; i < 2; i++)
            move();
        /*
        --- ohne Vorkenntnisse ---

        move();
        move();
        */
    }

    public void turnAround() {
        for (int i = 0; i < 2; i++)
            turnLeft();
        /*
        --- ohne Vorkenntnisse ---

        turnLeft();
        turnLeft();
        */
    }
}
