import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class World2a_RandomCarrots extends WorldRabbitWorld {

    public World2a_RandomCarrots() {
        prepare();
    }

    private void prepare() {
        RandomCarrot randomcarrot = new RandomCarrot(0.5);
        addObject(randomcarrot, 3, 3);
        RandomCarrot randomcarrot2 = new RandomCarrot(0.5);
        addObject(randomcarrot2, 4, 3);

        /* Aufgabe 6
        addObject(new CarrotRabbit1(), 0, 3);
        */
        /* Aufgabe 7
        addObject(new CarrotRabbit2(), 0, 3);
        */
    }
}
