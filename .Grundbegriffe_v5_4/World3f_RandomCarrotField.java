/**
 * Write a description of class ZufallsKarottenfeld here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class World3f_RandomCarrotField extends WorldRabbitWorld
{

   public World3f_RandomCarrotField(){
        super();
    }
	
    /***
     * Bevölkert die Welt, hier werden Karotten, Steine und Hasen erstellt und plaziert
     */
    public void populate(){
        addObject(new Rock(),6,6);
        addObject(new RandomCarrot(0.9),3,3);
        addObject(new RandomCarrot(0.9),4,3);
        addObject(new RandomCarrot(0.1),5,3);
        
        addObject(new RandomCarrot(0.5),3,1);
        addObject(new RandomCarrot(0.5),4,1);
        addObject(new RandomCarrot(0.5),5,1);
        
        addObject(new RandomCarrot(0.5),3,2);
        addObject(new RandomCarrot(0.5),4,2);
        addObject(new RandomCarrot(0.5),5,2);
        
        
        
        
    }
    
}
