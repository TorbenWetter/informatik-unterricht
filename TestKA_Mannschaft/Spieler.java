public class Spieler {

    private String name;
    private float gehalt;
    private boolean stamm;

    public Spieler(String name, float gehalt, boolean stamm) {
        this.name = name;
        this.gehalt = gehalt >= 0 ? gehalt : 0;
        this.stamm = stamm;
    }

    public String getName() {
        return name;
    }

    public float getGehalt() {
        return gehalt;
    }

    public boolean isStamm() {
        return stamm;
    }
}
