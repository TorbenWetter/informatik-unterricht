public class Main {

    public static void main(String[] args) {
        Mannschaft mannschaft = new Mannschaft("1. FC Tulpe");
        Spieler spieler = new Spieler("Hannes", 100, false);

        mannschaft.neuerSpieler(spieler);
        mannschaft.neuerSpieler(new Spieler("Andrea", 500.52f, true));
        System.out.println(mannschaft.berechneGehaltskosten());
    }
}
