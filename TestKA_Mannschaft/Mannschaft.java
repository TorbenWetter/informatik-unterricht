public class Mannschaft {

    private String name;
    private Spieler[] spieler;

    public Mannschaft(String name) {
        this.name = name;
        spieler = new Spieler[23];
    }

    public float berechneGehaltskosten() {
        float sum = 0;
        for (Spieler spieler : this.spieler) {
            if (spieler == null) {
                break;
            }
            if (spieler.isStamm()) {
                sum += spieler.getGehalt();
            } // you could also add half of the gehalt for not stamm-Spieler..
        }
        return sum;
    }

    public void neuerSpieler(Spieler spieler) {
        for (int i = 0; i < this.spieler.length; i++) {
            if (this.spieler[i] == null) {
                this.spieler[i] = spieler;
                return;
            }
        }
        System.out.println("Die Mannschaft ist voll.");
    }
}
