import other.Collatz;
import other.OtherAlgorithm;
import other.PythagoreanTriple;
import sort.BubbleSort;
import sort.QuickSort;
import sort.SelectionSort;
import sort.SortingAlgorithm;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Random;

public class Main {

    private static boolean contains(int[] arr, int elem) {
        for (int e : arr) {
            if (e == elem) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        //executeSortingAlgorithm(new QuickSort());

        //measureSortingRuntime(); // loops through all sorting algorithms

        //executeOtherAlgorithm(new Collatz(), 19);

        /*System.out.println(contains(new int[]{12, 15, 1, 22, 99, 8}, 33));
        System.out.println(contains(new int[]{12, 15, 1, 22, 99, 8}, 15));*/

        List<PythagoreanTriple.Triple> triples = new PythagoreanTriple().execute(100);
        PythagoreanTriple.filterForPrimitives(triples);
        System.out.println(triples);
    }

    private static void executeSortingAlgorithm(SortingAlgorithm algorithm) {
        int[] toSort1 = new int[]{};
        int[] toSort2 = new int[]{1};
        int[] toSort3 = new int[]{0, 0, 0};
        int[] toSort4 = new int[]{1, 2, 3, 4, 5, 6};
        int[] toSort5 = new int[]{6, 5, 4, 3, 2, 1};
        int[] toSort6 = new int[]{1, 8, 3, 5, 5, 6};
        int[] toSort7 = new int[]{1, 8, Integer.MAX_VALUE, 5, 5, 6};
        int[] toSort8 = generateRandomIntArray(200);
        int[] toSort9 = generateRandomIntArray(100000);

        int[][] arrays = new int[][]{toSort1, toSort2, toSort3, toSort4, toSort5, toSort6, toSort7, toSort8, toSort9};
        for (int i = 0; i < arrays.length; i++) {
            int[] arr = arrays[i];

            printArray(arr);

            long startTime = System.currentTimeMillis();
            algorithm.sort(arr);
            long timePassed = System.currentTimeMillis() - startTime;

            printArray(arr);

            System.out.println(String.format("Sorting (%s) array %d took %sms", algorithm.getClass().getSimpleName(), i + 1, timePassed));

            System.out.println("-------------------");
        }
    }

    private static void measureSortingRuntime() {
        // get all SortAlgorithms TODO: always add new ones
        SortingAlgorithm[] algorithms = new SortingAlgorithm[]{new BubbleSort(), new SelectionSort(), new QuickSort()};

        String fileName = "SortRuntimeAnalysis.csv";
        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName), StandardCharsets.UTF_8))) {
            writer.write("Method,ElementAmount,TimeMS");
            writer.newLine();
            for (SortingAlgorithm algorithm : algorithms) {
                for (int i = 1; i <= 25; i++) {
                    int n = 1000 * i;
                    int[] arr = generateRandomIntArray(n);

                    long startTime = System.nanoTime();
                    algorithm.sort(arr);
                    long endTime = System.nanoTime();
                    long ms = Math.round((double) (endTime - startTime) / 1000000);

                    writer.write(String.format("%s,%d,%d", algorithm.getClass().getSimpleName(), n, ms));
                    writer.newLine();
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        MyCSVPlotter.plot(fileName);
    }

    private static int[] generateRandomIntArray(int size) {
        int[] arr = new int[size];
        Random random = new Random(System.currentTimeMillis());
        for (int i = 0; i < arr.length; i++) {
            arr[i] = random.nextInt(1000);
        }
        return arr;
    }

    private static void printArray(int[] arr) {
        StringBuilder str = new StringBuilder();
        str.append("[");
        for (int elem : arr) {
            str.append(elem).append(", ");
        }
        if (str.length() > 1) {
            str.replace(str.length() - 2, str.length(), "");
        }
        str.append("]");
        System.out.println(str.toString());
    }

    private static void executeOtherAlgorithm(OtherAlgorithm otherAlgorithm, int n) {
        long startTime = System.currentTimeMillis();
        Object result = otherAlgorithm.execute(n);
        long timePassed = System.currentTimeMillis() - startTime;
        StringBuilder additionalInfo = new StringBuilder();
        if (result instanceof List) {
            additionalInfo.append(String.format("size: %d, ", ((List) result).size()));
        }
        System.out.println(String.format("%s with %d: %s, %stime: %dms", otherAlgorithm.getClass().getSimpleName(), n, result, additionalInfo.toString(), timePassed));
    }
}