package other;

public class Factorial implements OtherAlgorithm<Long> {

    private Long fact(int n) {
        if (n <= 1) {
            return 1L;
        }
        return fact(n - 1) * n;
    }

    @Override
    public Long execute(int n) {
        if (n < 0 || n > 20) { // the factorial of 20 is the limit of what a Long can save
            return -1L;
        }
        return fact(n);
    }
}
