package other;

import java.util.LinkedList;

public class Collatz implements OtherAlgorithm<LinkedList<Integer>> {

    private LinkedList<Integer> collatz(int n, LinkedList<Integer> list) {
        list.addLast(n);
        if (n == 1) {
            return list;
        }

        if (n % 2 == 0) {
            n /= 2;
        } else {
            n = 3 * n + 1;
        }
        return collatz(n, list);
    }

    @Override
    public LinkedList<Integer> execute(int n) {
        LinkedList<Integer> list = new LinkedList<>();
        if (n < 1) { // the given number has to be a natural one
            return list;
        }
        return collatz(n, list);
    }
}
