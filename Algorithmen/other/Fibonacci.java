package other;

public class Fibonacci implements OtherAlgorithm<Long> {

    private Long fib(int n, Long[] arr) {
        Long potFib = arr[n];
        if (potFib != null) {
            return potFib;
        }

        Long fib = fib(n - 1, arr) + fib(n - 2, arr);
        arr[n] = fib;
        return fib;
    }

    @Override
    public Long execute(int n) {
        if (n < 0 || n > 1754) { // the 1754th fibonacci number (starting at 0) is the limit of what a Long can save
            return -1L;
        }
        Long[] arr = new Long[n + 1];
        arr[0] = 1L;
        if (arr.length > 1) {
            arr[1] = 1L;
        }
        return fib(n, arr);
    }
}
