package other;

public interface OtherAlgorithm<T> {

    public T execute(int n);
}
