package other;

import java.util.LinkedList;
import java.util.List;

public class PythagoreanTriple implements OtherAlgorithm<List<PythagoreanTriple.Triple>> {

    public class Triple {
        public int x, y, z;

        public Triple(int x, int y, int z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        @Override
        public String toString() {
            return String.format("%d²+%d²=%d²", this.x, this.y, this.z);
        }
    }

    @Override
    public List<Triple> execute(int maxZ) {
        List<Triple> triples = new LinkedList<>();
        for (int x = 1; x * x < maxZ * maxZ; x++) {
            for (int y = x; (x * x) + (y * y) <= maxZ * maxZ; y++) {
                int z = Math.round((float) Math.sqrt((x * x) + (y * y)));
                if ((x * x) + (y * y) == z * z) {
                    triples.add(new Triple(x, y, z));
                }
            }
        }
        return triples;
    }

    private static int greatestCommonDivisor(int a, int b) {
        if (a == 0 || b == 0) {
            return a + b;
        }
        return greatestCommonDivisor(b, a % b);
    }

    public static void filterForPrimitives(List<Triple> triples) {
        triples.removeIf(triple -> {
            int gcm = greatestCommonDivisor(triple.x, greatestCommonDivisor(triple.y, triple.z));
            int reducedX = triple.x / gcm, reducedY = triple.y / gcm, reducedZ = triple.z / gcm;
            for (Triple otherTriple : triples) {
                if (reducedX == otherTriple.x && reducedY == otherTriple.y && reducedZ == otherTriple.z && triple != otherTriple) {
                    return true;
                }
            }
            return false;
        });
    }
}
