package sort;

public class SelectionSort implements SortingAlgorithm {

    private static boolean isEmpty(int[] arr) {
        for (int i : arr) {
            if (i != Integer.MAX_VALUE) {
                return false;
            }
        }
        return true;
    }

    private static int minIndex(int[] arr) {
        int indexMin = -1;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != Integer.MAX_VALUE) {
                if (indexMin == -1 || arr[i] < arr[indexMin]) {
                    indexMin = i;
                }
            }
        }
        return indexMin;
    }

    // first version (with creating a new array)
    public static void selectionSortIntArray(int[] arr) {
        int[] sorted = new int[arr.length];
        int insertIndex = 0;
        while (!isEmpty(arr)) {
            int indexMin = minIndex(arr);
            sorted[insertIndex] = arr[indexMin];
            arr[indexMin] = Integer.MAX_VALUE;
            insertIndex++;
        }

        // if there were Integer.MAX_VALUE's in the original array, insert them again because they were skipped
        for (int i = 0; i < arr.length - insertIndex; i++) {
            sorted[insertIndex + i] = Integer.MAX_VALUE;
        }

        // copy values of sorted to arr without changing the reference..
        System.arraycopy(sorted, 0, arr, 0, arr.length);
    }

    private int minIndex(int[] arr, int startIndex) {
        int indexMin = -1;
        if (startIndex < 0 || startIndex > arr.length - 1) {
            return indexMin;
        }

        for (int i = startIndex; i < arr.length; i++) {
            if (indexMin == -1 || arr[i] < arr[indexMin]) {
                indexMin = i;
            }
        }
        return indexMin;
    }

    @Override
    public void sort(int[] arr) {
        int unsortedStart = 0; // unsorted part starts from index, sorted part is everything else
        while (unsortedStart < arr.length - 1) {
            int indexMin = minIndex(arr, unsortedStart);
            int temp = arr[indexMin];
            arr[indexMin] = arr[unsortedStart];
            arr[unsortedStart] = temp;
            unsortedStart++;
        }
    }
}
