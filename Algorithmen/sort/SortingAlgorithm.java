package sort;

public interface SortingAlgorithm {

    public void sort(int[] arr);
}
