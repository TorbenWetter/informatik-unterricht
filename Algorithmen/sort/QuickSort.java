package sort;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.stream.Collectors;

public class QuickSort implements SortingAlgorithm {

    private LinkedList<Integer> sort(LinkedList<Integer> list) {
        if (list.size() < 2) {
            return list;
        }

        Integer pivot = list.pollFirst();
        LinkedList<Integer> smallerList = new LinkedList<>();
        LinkedList<Integer> largerList = new LinkedList<>();
        for (Integer element : list) {
            if (element < pivot) {
                smallerList.addLast(element);
            } else {
                largerList.addLast(element);
            }
        }
        LinkedList<Integer> sortedList = sort(smallerList);
        sortedList.addLast(pivot);
        sortedList.addAll(sort(largerList));
        return sortedList;
    }

    @Override
    public void sort(int[] arr) {
        LinkedList<Integer> list = Arrays.stream(arr).boxed().collect(Collectors.toCollection(LinkedList::new));

        int[] sorted = sort(list).stream().mapToInt(i -> i).toArray();
        System.arraycopy(sorted, 0, arr, 0, arr.length);
    }
}
