package sort;

import de.gmo.sae.sorting.visualization.ArrayVisualizer;

public class BubbleSort implements SortingAlgorithm {

    private void swapByIndex(int[] arr, int ind1, int ind2) {
        if (ind1 < 0 || ind1 >= arr.length || ind2 < 0 || ind2 >= arr.length) {
            System.out.println("Invalid index!");
            return;
        }

        int temp = arr[ind1];
        arr[ind1] = arr[ind2];
        arr[ind2] = temp;
    }

    @Override
    public void sort(int[] arr) {
        if (arr.length <= 1) {
            return;
        }

        //ArrayVisualizer visualizer = new ArrayVisualizer(arr);
        int swapOperations = 0, iterations = 0;
        boolean swapped;
        do {
            swapped = false;
            for (int i = 0; i < arr.length - 1 - iterations; i++) {
                // if the two compared elements are descending instead of ascending, swap them
                if (arr[i] > arr[i + 1]) {
                    // no need to return anything (method call by reference)
                    swapByIndex(arr, i, i + 1);
                    //visualizer.visualize();
                    swapOperations++;
                    swapped = true;
                }
            }
            iterations++;
        } while (swapped);

        //System.out.println(String.format("swapOperations: %s", swapOperations));
    }
}
