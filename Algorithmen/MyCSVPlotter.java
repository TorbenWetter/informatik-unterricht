import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class MyCSVPlotter extends Application {

    private static String fileName;

    @Override
    public void start(Stage stage) {
        stage.setTitle(fileName);

        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        final LineChart<Number, Number> lineChart = new LineChart<>(xAxis, yAxis);

        Map<String, List<XYChart.Data<Number, Number>>> serieses = new HashMap<>();
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            boolean firstLine = true;
            String line;
            while ((line = br.readLine()) != null) {
                String[] lineParts = line.split(",");

                if (firstLine) {
                    firstLine = false;
                    xAxis.setLabel(lineParts[1]);
                    yAxis.setLabel(lineParts[2]);
                    continue;
                }

                String key = lineParts[0], x = lineParts[1], y = lineParts[2];
                XYChart.Data<Number, Number> coordinates = new XYChart.Data<>(Double.valueOf(x), Double.valueOf(y));
                if (!serieses.containsKey(key)) {
                    List<XYChart.Data<Number, Number>> list = new LinkedList<>();
                    list.add(coordinates);
                    serieses.put(key, list);
                } else {
                    serieses.get(key).add(coordinates);
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        Scene scene = new Scene(lineChart, 800, 600);

        for (String seriesKey : serieses.keySet()) {
            XYChart.Series<Number, Number> series = new XYChart.Series<>();
            series.setName(seriesKey);
            series.getData().addAll(serieses.get(seriesKey));
            lineChart.getData().add(series);
        }

        stage.setScene(scene);
        stage.show();
    }

    public static void plot(String fileName) {
        MyCSVPlotter.fileName = fileName;
        launch();
    }
}