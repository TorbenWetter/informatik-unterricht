import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;

public class Main3 {

    public static void main(String[] args) {
        measurePrimeDecompositionRuntime();
    }

    private static void measurePrimeDecompositionRuntime() {
        String fileName = "PrimeDecompositionRuntimeAnalysis.csv";
        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName), StandardCharsets.UTF_8))) {
            writer.write("Method,Number,TimeMS");
            writer.newLine();
            BigInteger step = new BigInteger("100000");
            for (int i = 0; i < 100; i++) {
                System.out.println(i);
                // n = i * step + (int) Math.floor(Math.random() * step)
                BigInteger n = new BigInteger(String.valueOf(i)).multiply(step).add(new BigDecimal(String.valueOf(Math.random())).multiply(new BigDecimal(step)).toBigInteger());
                long startTime = System.nanoTime();
                PrimeDecomposition.getDecomposedPrimes(n);
                long endTime = System.nanoTime();
                double ms = (double) (endTime - startTime) / 1000000;

                writer.write(String.format("Standard,%d,%f", n, ms));
                writer.newLine();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        MyCSVPlotter.plot(fileName);
    }
}

