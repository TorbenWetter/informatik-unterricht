import java.math.BigInteger;
import java.util.*;

public class PrimeDecomposition {

    public static List<BigInteger> getDecomposedPrimes(BigInteger number) {
        List<BigInteger> primes = getPrimes(number.sqrt());
        return getDecomposedPrimes(number, primes, new LinkedList<>());
    }

    private static List<BigInteger> getDecomposedPrimes(BigInteger number, List<BigInteger> primes, List<BigInteger> decomposedPrimes) {
        boolean foundNewPrime = false;
        for (BigInteger prime : primes) {
            // number % prime == 0
            if (number.mod(prime).compareTo(new BigInteger("0")) == 0) {
                number = number.divide(prime);
                decomposedPrimes.add(prime);
                foundNewPrime = true;
                break;
            }
        }

        if (!foundNewPrime) {
            // number != 1
            if (number.compareTo(new BigInteger("1")) != 0) {
                decomposedPrimes.add(number);
            }
            return decomposedPrimes;
        }
        return getDecomposedPrimes(number, primes, decomposedPrimes);
    }

    // https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes
    private static List<BigInteger> getPrimes(BigInteger maxNumber) {
        List<BigInteger> primes = new LinkedList<>();
        // error case (maxNumber < 2)
        if (maxNumber.compareTo(new BigInteger("2")) < 0) {
            return primes;
        }

        // store marked n's (which are no primes)
        List<BigInteger> marked = new LinkedList<>();
        // for (BigInteger i = 0; i < maxNumber - 1; i++) ..
        for (BigInteger i = new BigInteger("0"); i.compareTo(maxNumber.subtract(new BigInteger("1"))) < 0; i = i.add(new BigInteger("1"))) {
            BigInteger n = i.add(new BigInteger("2"));
            // always work with first unmarked n -> go to next n if already marked as false
            if (marked.contains(n)) {
                continue;
            }

            // found a prime
            primes.add(n);

            // mark multiples of n as false
            // for (BigInteger j = i; j < maxNumber - 1; j += n) ..
            for (BigInteger j = i; j.compareTo(maxNumber.subtract(new BigInteger("1"))) < 0; j = j.add(n)) {
                marked.add(n);
            }
        }
        return primes;
    }
}
