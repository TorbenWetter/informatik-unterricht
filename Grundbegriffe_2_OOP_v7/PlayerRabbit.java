import greenfoot.*;
import java.util.List;

public class PlayerRabbit extends MovingActor {
    
    //Attribute
    
    
    
    //Konstruktoren
    
    
    //Methoden
    /**
     * Wird einmal pro Zeiteinheit aufgerufen
     */
    public void act() {
        performMovement();
    }


    //TODO: Steuerung �ber die Tasten W - A - S - D realisieren
    private void performMovement() {
        if (Greenfoot.isKeyDown("W")) {
            move();
        }                 
    }
    
    
     /**
     * moves one step forward
     */
    public void move(){
        move(1);
    }

}
