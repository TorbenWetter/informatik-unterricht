import greenfoot.Greenfoot;
import greenfoot.World;

public class Level2 extends World {

    public Level2(Player player) {
        super(10, 10, 60);
        setBackground("tiles/cell.jpg");
        setPaintOrder(Player.class);  //Player is painted last

        Greenfoot.setSpeed(35);

        this.addObject(player, 0, player.getY());

        this.addObject(new Apple(), 1, 4);
        this.addObject(new Apple(), 3, 1);
        this.addObject(new Apple(), 4, 7);

        this.addObject(new Banana(), 2, 6);
        this.addObject(new Banana(), 6, 1);

        this.addObject(new Carrot(), 5, 2);
        this.addObject(new Carrot(), 7, 0);
        this.addObject(new Carrot(), 2, 3);

        this.addObject(new Rock(), 5, 4);
        this.addObject(new Rock(), 6, 5);
        this.addObject(new Rock(), 8, 3);

        this.addObject(new Tree(), 1, 3);
        this.addObject(new Tree(), 6, 4);
    }
}