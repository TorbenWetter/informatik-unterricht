import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class Level1 extends World {

    /**
     * AB Aufgaben1 KlassenObjekte
     * Aufgabe 2: (Aufgabenstellung)
     * a) Informiere dich in der API über die Methode addObject(…). Welche Parameter und welchen
     * Rückgabetyp hat die Methode? Beschreibe, was passiert, wenn die Methode aufgerufen wird,
     * gehe hierbei insbesondere auf die einzelnen Parameter ein.
     * b) Erzeuge einen neuen Fels (Rock). Platziere den Fels auf (5, 5).
     * c) Erstelle eine Karotte mit dem Gewicht 12 und platziere sie auf dem Feld (1, 5).
     * d) Erstelle eine weitere Karotte mit dem Gewicht 3 und platziere sie auf dem Feld (1, 6).
     * e) Fülle die komplette 7. Spalte der Welt Level1 mit Felsen auf.
     * f) Gib den Unterschied zwischen einer Klasse und einem Objekt in eigenen Worten wieder.
     * Finde entsprechende Beispiele in deinem Quellcode.
     * <p>
     * a) Die Methode ist eine void. Parameter sind: der Actor und zwei ints (x- und y-Koordinate in der Welt)
     * Wenn das Objekt noch nicht in der Welt (auf die die Methode aufgerufen wird) ist, wird es aus evtl. anderen
     * Welten entfernt und an den angegebenen Koordinaten in die Welt gesetzt
     * <p>
     * b) - e)
     *
     * @see #Level1()
     * f) In einer Klasse werden (Klassen-)Methoden und Variablen geschrieben, die dann als Objekt instanziert werden kann.
     * Der Konstruktor einer Klasse wird bei Instanzierung (new Klasse()) aufgerufen.
     **/

    public Level1() {
        super(10, 10, 60);
        setBackground("tiles/cell.jpg");
        setPaintOrder(Player.class);  //Player is painted last

        Greenfoot.setSpeed(35);

        Player player = new Player();
        this.addObject(player, 4, 4);

        Rock rock = new Rock();
        addObject(rock, 5, 5); // ohne this. ...

        Carrot carrot1 = new Carrot(12);
        addObject(carrot1, 1, 5);

        Carrot carrot2 = new Carrot(3);
        addObject(carrot2, 1, 6);

        for (int i = 0; i < getHeight() - 1; i++) {
            if (i != 3) {
                addObject(new Rock(), 6, i);
            }
        }

        addObject(new Lever(false), 3, 3);
    }
}