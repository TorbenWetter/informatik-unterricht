import greenfoot.*;

/**
 * Class used to display an Array of Actors at the bottom of the screen.
 */
public class InventoryDisplayAdapter extends Actor {

    private int selectedSlot;
    private Actor[] items;
    private Actor[] itemImages;
    private int capacity;
    private GreenfootImage emtpySlot = new GreenfootImage("./images/tiles/EmptySlot.png");

    public InventoryDisplayAdapter() {
        this(1);
    }

    public InventoryDisplayAdapter(int initialCapacity) {
        getImage().setTransparency(0);
        this.capacity = initialCapacity;
    }

    /**
     * Method of Actor, will be called once the Adapter will be added/inserted (in)to the world.
     *
     * @param world target world
     */
    protected void addedToWorld(World world) {
        if (items == null) {
            setLinkedItems(new Actor[capacity]);
        }
    }

    public void act() {
        for (int i = 0; i < items.length; i++) {
            if (items[i] == null && itemImages[i].getImage() != emtpySlot) {
                itemImages[i].setImage(emtpySlot);
            } else if (items[i] != null && itemImages[i].getImage() != items[i].getImage()) {
                itemImages[i].setImage(items[i].getImage());
            }

        }
        GreenfootImage tempImage = new GreenfootImage(itemImages[selectedSlot].getImage());
        int size = getWorld().getCellSize();
        tempImage.drawRect(size / 4, size / 4, size / 2, size / 2);
        itemImages[selectedSlot].setImage(tempImage);
        // TODO: some images are not 60x60 -> square is at different location..
        // ----> resize images! (calculating offset is not possible because rect won't be drawn)
    }

    /**
     * Links an array of Items to the InventoryDisplayAdapter so it can be displayed inside the adapter.
     *
     * @param inventory array of Actors that shall be displayed
     */
    public void setLinkedItems(Actor[] inventory) {
        capacity = inventory.length;
        items = inventory;
        itemImages = new Actor[capacity];
        for (int i = 0; i < capacity; i++) {
            Actor dummyImageActor = createDummyImageActor(i);
            itemImages[i] = dummyImageActor;
        }
    }

    public void setSelectedSlot(int selectedSlot) {
        this.selectedSlot = selectedSlot;
    }

    private Actor createDummyImageActor(int i) {
        Actor actor = new Actor() {
        };
        getWorld().addObject(actor, i, getY());
        itemImages[i] = actor;
        actor.setImage(emtpySlot);

        return itemImages[i];
    }
}