/**
 * Just a Rock
 */
public class Rock extends Environment {

    private int life;

    public Rock() {
        this(3);
    }

    public Rock(int life) {
        this.life = life;
    }

    public boolean punch() {
        if (this.life == 1) {
            getWorld().removeObject(this);
            return true;
        }
        this.life--;
        return false;
    }

    public void act() {
        print(String.valueOf(this.life));
    }

    /**
     * Will be called on a hit.
     */
    public void hit() {
        getWorld().addObject(new Star(), getX(), getY()); // Bei einem Treffer wird kurz ein Stern eingeblendet
    }

    @Override
    public boolean isBlocking() {
        return true;
    }
}