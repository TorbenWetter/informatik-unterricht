public interface UsableObject {

    public void use(Player player);
}
