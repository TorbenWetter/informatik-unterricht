public class Apple extends Environment implements PickableObject, UsableObject {

    @Override
    public boolean isBlocking() {
        return false;
    }

    @Override
    public void pick() {
        System.out.println("Apple aufgehoben");
    }

    @Override
    public void use(Player player) {
        player.addLife(10);
        getWorld().removeObject(this);
    }
}
