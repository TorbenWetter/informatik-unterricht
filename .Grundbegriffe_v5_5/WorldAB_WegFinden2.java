import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class WorldAB_WegFinden2 here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class WorldAB_WegFinden2 extends WorldRabbitWorld
{

    /**
     * Constructor for objects of class WorldAB_WegFinden2.
     * 
     */
    public WorldAB_WegFinden2()
    {
        super(10,10);   
        prepare();
    }

    /**
     * Bereite die Welt f�r den Programmstart vor.
     * Das hei�t: Erzeuge die Anfangs-Objekte und f�ge sie der Welt hinzu.
     */
    private void prepare()
    {
        Rock rock37 = new Rock();
        addObject(rock37,8,7);
        Rock rock38 = new Rock();
        addObject(rock38,7,7);
        Rock rock39 = new Rock();
        addObject(rock39,8,9);
        Rock rock40 = new Rock();
        addObject(rock40,7,9);
        Rock rock41 = new Rock();
        addObject(rock41,6,9);
        Rock rock42 = new Rock();
        addObject(rock42,5,9);
        Rock rock43 = new Rock();
        addObject(rock43,5,8);
        Rock rock44 = new Rock();
        addObject(rock44,5,7);
        Rock rock45 = new Rock();
        addObject(rock45,5,6);
        Rock rock46 = new Rock();
        addObject(rock46,7,6);
        Rock rock47 = new Rock();
        addObject(rock47,7,5);
        Rock rock48 = new Rock();
        addObject(rock48,7,4);
        Rock rock49 = new Rock();
        addObject(rock49,6,4);
        Rock rock50 = new Rock();
        addObject(rock50,5,4);
        Rock rock51 = new Rock();
        addObject(rock51,4,4);
        Rock rock52 = new Rock();
        addObject(rock52,3,4);
        Rock rock53 = new Rock();
        addObject(rock53,4,6);
        Rock rock54 = new Rock();
        addObject(rock54,2,4);
        Rock rock55 = new Rock();
        addObject(rock55,1,4);
        Rock rock56 = new Rock();
        addObject(rock56,1,5);
        Rock rock57 = new Rock();
        addObject(rock57,1,6);
        Rock rock58 = new Rock();
        addObject(rock58,3,7);
        Rock rock59 = new Rock();
        addObject(rock59,3,6);
        Rock rock60 = new Rock();
        addObject(rock60,1,7);
        Rock rock61 = new Rock();
        addObject(rock61,1,8);
        Rock rock62 = new Rock();
        addObject(rock62,3,8);
        Carrot carrot2 = new Carrot();
        addObject(carrot2,2,9);
        Rock rock63 = new Rock();
        addObject(rock63,9,7);
        Rock rock64 = new Rock();
        addObject(rock64,9,9);
        
        MazeRabbit mazeRabbit = new MazeRabbit();
        addObject(mazeRabbit, 9, 8);
        mazeRabbit.turnAround();
    }
}
