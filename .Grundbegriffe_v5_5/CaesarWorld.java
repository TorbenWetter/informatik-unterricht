import greenfoot.World;

public class CaesarWorld extends WorldRabbitWorld {

    public CaesarWorld() {
        super();
        prepare();
    }

    private void prepare() {
        addObject(new CaesarRabbit(), 0, 0);
    }
}
