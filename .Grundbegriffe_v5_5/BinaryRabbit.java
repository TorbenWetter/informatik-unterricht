import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class BinaryRabbit extends CarefulRabbit {

    public void init() {
        /* 05d Aufgabe 2 a)
       turnAround();
       move();
       checkForStone(0);
        */
    }
    
    private void checkForStone(int fieldsMoved) {
        turnRight();
        if (canMove()) {
            turnRight();
            for (int i = 0; i < fieldsMoved + 1; i++) {
                move();
            }
            return;
        }
        turnLeft();
        replaceCarrot();
        move();
        checkForStone(fieldsMoved + 1);
    }
    
    private void replaceCarrot() {
        if (isCarrotNear()) {
            pickCarrot();
        } else {
            putCarrot();
        }
    }
}
