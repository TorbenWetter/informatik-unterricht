import greenfoot.Greenfoot;

public class HueArrayRabbit extends CarefulRabbit {

    public void init() {
        //int[] array1 = sortedArray(5);
        int[] array1 = {1, 2, 99, 200};
        print(array1);

        //int[] array2 = sortedArray(5);
        int[] array2 = {3, 5, 60, 77, 91};
        print(array2);

        int[] mergedArray = merge(array1, array2);
        print(mergedArray);
    }

    public int[] merge(int[] first, int[] second) {
        int[] merged = new int[first.length + second.length];
        int firstIndex = 0, secondIndex = 0;
        for (int i = 0; i < merged.length; i++) {
            if (firstIndex >= first.length) {
                merged[i] = second[secondIndex];
                secondIndex++;
            } else if (secondIndex >= second.length) {
                merged[i] = first[firstIndex];
                firstIndex++;
            } else if (first[firstIndex] > second[secondIndex]) {
                merged[i] = second[secondIndex];
                secondIndex++;
            } else {
                merged[i] = first[firstIndex];
                firstIndex++;
            }
        }
        return merged;
    }

    public int[] sortedArray(int size) {
        int[] sorted = new int[size];
        sorted[0] = Greenfoot.getRandomNumber(50);
        for (int i = 1; i < size; i++) {
            sorted[i] = sorted[i - 1] + Greenfoot.getRandomNumber(50);
        }
        return sorted;
    }

    public void print(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + ", ");
        }
        System.out.println();
    }
}
