import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class EigeneAufgabe here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class EigeneAufgabe extends ArrayBugsy
{
    /**
     * Act - do whatever the EigeneAufgabe wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void init() 
    {
        lottoZiehen();
    }
    public int[] lottoZiehen(){
        int[] lottoZiehung = new int[7];                //LottoArray erstellen und deklarieren
        boolean[] doubleNumbersDeny = new boolean[49];  //Wahrheitswert-Array erstellen (beachte: jede Zelle bekommt zu Anfang false zugewiesen)
        int rdmNumber = 0;                              //Selbsterklärend
        for(int i = 0;i < lottoZiehung.length-1;i++){   //Die LottoZiehung
            rdmNumber = getRandomNumber(1,49);          //Ist denke ich auch Selbsterklärend
            if(doubleNumbersDeny[rdmNumber] == false){  //Hier wird abgefragt ob die Zelle mit der jeweiligen rdmNumber false ist, wäre sie true würde der darunterliegende Code nicht ausgeführt werden
                lottoZiehung[i] = rdmNumber;            //Hier wird der jewiligen Zelle der LottoZiehung der rdmNumber-Wert zugewiesen
                doubleNumbersDeny[rdmNumber] = true;    //Und hier wird die vorher abgefragte Zelle von doubleNumbersDeny auf true gesetzt damit dieser Wert nicht nochmal gezogen wird
            }
        } 
        ausgabe(lottoZiehung);                          //Zur Übersichtlichkeit unten definiert }
        ausgabe2(doubleNumbersDeny);                    //Zur Übersichtlichkeit unten definiert  > Hier Gibt es ein Problem welches ich nicht finden konnte, es wird nach den 7 Lotto Zahlen immer noch eine null mit ausgegeben
        return lottoZiehung;                            //                                      }
    }
    
    public int getRandomNumber(int start,int end)       
    {
        int normal = Greenfoot.getRandomNumber(end-start+1);
        return normal+start;
    }
    
    public int[] ausgabe(int[] lottoZiehung){
        for(int i = 0;i < lottoZiehung.length; i++){
            System.out.print(lottoZiehung[i]);
            System.out.print(",");
        }
        System.out.println("");
        return lottoZiehung;
    }
    
    public boolean[] ausgabe2(boolean[] doubleNumbersDeny){
        for(int i = 0;i < doubleNumbersDeny.length; i++){
            System.out.print(doubleNumbersDeny[i]);
            System.out.print(",");
        }
        System.out.println("");
        return doubleNumbersDeny;
    }
    
}
