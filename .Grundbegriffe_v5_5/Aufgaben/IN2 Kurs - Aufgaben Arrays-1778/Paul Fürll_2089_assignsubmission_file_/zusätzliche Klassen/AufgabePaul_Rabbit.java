import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class AufgabePaul_Rabbit here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class AufgabePaul_Rabbit extends CarefulRabbit
{
    /**
     * Act - do whatever the AufgabePaul_Rabbit wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void init() 
    {   
        // dein Code...
    }
    public void removeAllCarrots() {
        Greenfoot.delay(1);
        getWorld().removeObjects(getWorld().getObjects(Carrot.class));
        Greenfoot.delay(1);
    }
    public void putCarrotMatrix(boolean[][] matrix) {
        World w=getWorld();
        for(int y=0;y<matrix.length;y++){
            for(int x=0;x<matrix[y].length;x++){
                if(matrix[x][y]) {
                    w.addObject(new Carrot(),x,y);
                }
            }
        }
        Greenfoot.delay(6);
    }
    public int worldWidth() {
        return getWorld().getWidth();
    }
    public int worldHeight() {
        return getWorld().getHeight();
    }
}
