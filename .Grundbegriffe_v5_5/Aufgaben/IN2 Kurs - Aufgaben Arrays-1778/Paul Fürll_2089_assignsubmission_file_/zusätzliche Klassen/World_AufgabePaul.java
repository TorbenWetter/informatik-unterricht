import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class World_AufgabePaul here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class World_AufgabePaul extends WorldRabbitWorld
{

    /**
     * Constructor for objects of class World_AufgabePaul.
     * 
     */
    public World_AufgabePaul()
    {
        super(9,9);

        prepare();
    }
    private void prepare()
    {
        Rabbit r = new AufgabePaul_Rabbit();
        addObject(r, 0, 0);
        addObject(new Carrot(),1,4);
        addObject(new Carrot(),2,3);
        addObject(new Carrot(),2,5);
        addObject(new Carrot(),3,2);
        addObject(new Carrot(),3,6);
        addObject(new Carrot(),4,2);
        addObject(new Carrot(),4,3);
        addObject(new Carrot(),4,4);
        addObject(new Carrot(),4,5);
        addObject(new Carrot(),4,6);
        addObject(new Carrot(),5,2);
        addObject(new Carrot(),5,6);
        addObject(new Carrot(),6,2);
        addObject(new Carrot(),6,5);
        addObject(new Carrot(),6,6);
        addObject(new Carrot(),7,2);
        addObject(new Carrot(),7,5);
        addObject(new Carrot(),7,6);
        addObject(new Carrot(),8,0);
        addObject(new Carrot(),8,1);
        addObject(new Carrot(),8,2);
        addObject(new Carrot(),8,3);
        addObject(new Carrot(),8,4);
        addObject(new Carrot(),8,5);
        addObject(new Carrot(),8,6);
        addObject(new Carrot(),8,7);
        addObject(new Carrot(),8,8);
        
    }
}
