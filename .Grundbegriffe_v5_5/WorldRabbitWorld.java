
import java.awt.Color;

// imports Actor, World, Greenfoot, GreenfootImage
import greenfoot.Actor;
import greenfoot.Greenfoot;
import greenfoot.GreenfootImage;
import greenfoot.World;
/**
 * @author Michael Kolling, changed by Thomas Karp
 * @version 1.0.3
 */
public abstract class WorldRabbitWorld extends World 
{	
	private static WorldRabbitWorld instance = null;
    private static int pauseStep = 15;
    
    public static WorldRabbitWorld getInstance(){
        if (instance == null){
            throw new RuntimeException("World not yet initialized!");
        }
        return instance;
    }

    /**
     * Erzeuge eine Welt.
     */
    public WorldRabbitWorld(int x, int y) 
    {
        super(x, y, 60);  
        load(this);
        setBackground("cell.jpg");
        setPaintOrder(Rabbit.class, Carrot.class, Rock.class);
    }
    
    /**
     * Erzeuge eine Welt.
     */
    public WorldRabbitWorld() 
    {
       this(8,8);
        
    }	

    
    protected static void load(WorldRabbitWorld world){
        instance = world;
        Greenfoot.setWorld(world);
    }

       
    public static void sleep() {
        Greenfoot.delay((int)pauseStep);
    }

    public void alert(String text){
        PopupMessage msg = new PopupMessage(text, 36, Color.red, Color.white, Color.white);
        addObject(msg, getWidth()/2, getHeight()/2);

    }   
} 

class PopupMessage extends Actor{
        
    public PopupMessage(String text, int size, Color textColor, Color background, Color edge){
       GreenfootImage img = new GreenfootImage(text, size, textColor, background, edge);
       img.setTransparency(95);
       setImage(img);
    }
}