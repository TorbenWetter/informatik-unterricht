
public class World0 extends WorldRabbitWorld {
    public World0(){
        super();

        prepare();
    }


    /**
     * Bereite die Welt f�r den Programmstart vor. Das hei�t: Erzeuge die Anfangs-
     * Objekte und f�ge sie der Welt hinzu.
     */
    private void prepare()
    {
        addObject(new NormalRabbit(), 1, 3);        
    }
}
