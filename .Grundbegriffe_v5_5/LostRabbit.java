import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class LostRabbit extends CarefulRabbit {

    public void init() {
        /* 05d Aufgabe 1 a)
        searchHome();
        */
    }

    private void searchHome() {
        if (!isCarrotNear()) {
            return;
        }
        turnLeft();
        if (!canMove()) {
            return;
        }
        turnAround();
        if (!canMove()) {
            return;
        }
        turnLeft();
        move();
        searchHome();
    }
}
