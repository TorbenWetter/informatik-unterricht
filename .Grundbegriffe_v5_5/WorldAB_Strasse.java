import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class WorldAB_Strasse here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class WorldAB_Strasse extends WorldRabbitWorld
{

    /**
     * Constructor for objects of class WorldAB_Strasse.
     * 
     */
    public WorldAB_Strasse()
    {
        super(10,10);
        prepare();
    }

    /**
     * Bereite die Welt f�r den Programmstart vor.
     * Das hei�t: Erzeuge die Anfangs-Objekte und f�ge sie der Welt hinzu.
     */
    private void prepare()
    {
        Carrot carrot = new Carrot();
        addObject(carrot,1,5);
        Carrot carrot2 = new Carrot();
        addObject(carrot2,2,5);
        Carrot carrot3 = new Carrot();
        addObject(carrot3,3,5);
        Carrot carrot4 = new Carrot();
        addObject(carrot4,4,5);
        Carrot carrot5 = new Carrot();
        addObject(carrot5,5,5);
        Carrot carrot6 = new Carrot();
        addObject(carrot6,6,5);
        Carrot carrot7 = new Carrot();
        addObject(carrot7,7,5);
        RandomRock rock = new RandomRock(0.5);
        addObject(rock,5,6);
        
        addObject(new LostRabbit(), 1, 5);
    }
}
