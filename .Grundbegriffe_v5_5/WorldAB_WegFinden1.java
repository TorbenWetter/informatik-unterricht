import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class WorldAB_Wegfinden here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class WorldAB_WegFinden1 extends WorldRabbitWorld
{

    /**
     * Constructor for objects of class WorldAB_Wegfinden.
     * 
     */
    public WorldAB_WegFinden1()
    {
        super(10,10);
        prepare();
    }

    /**
     * Bereite die Welt f�r den Programmstart vor.
     * Das hei�t: Erzeuge die Anfangs-Objekte und f�ge sie der Welt hinzu.
     */
    private void prepare()
    {
        Rock rock = new Rock();
        addObject(rock,0,0);
        Rock rock2 = new Rock();
        addObject(rock2,0,1);
        Rock rock3 = new Rock();
        addObject(rock3,0,2);
        Rock rock4 = new Rock();
        addObject(rock4,0,3);
        Rock rock5 = new Rock();
        addObject(rock5,0,4);
        Rock rock6 = new Rock();
        addObject(rock6,0,5);
        Rock rock7 = new Rock();
        addObject(rock7,2,0);
        Rock rock8 = new Rock();
        addObject(rock8,0,9);
        Rock rock9 = new Rock();
        addObject(rock9,1,9);
        Rock rock10 = new Rock();
        addObject(rock10,2,9);
        Rock rock11 = new Rock();
        addObject(rock11,3,9);
        Rock rock12 = new Rock();
        addObject(rock12,4,9);
        Rock rock13 = new Rock();
        addObject(rock13,0,7);
        Rock rock14 = new Rock();
        addObject(rock14,1,7);
        Rock rock15 = new Rock();
        addObject(rock15,3,7);
        Rock rock16 = new Rock();
        addObject(rock16,2,7);
        Rock rock17 = new Rock();
        addObject(rock17,4,7);
        Rock rock18 = new Rock();
        addObject(rock18,5,9);
        Rock rock19 = new Rock();
        addObject(rock19,6,9);
        Rock rock20 = new Rock();
        addObject(rock20,6,8);
        Rock rock21 = new Rock();
        addObject(rock21,6,7);
        Rock rock22 = new Rock();
        addObject(rock22,4,6);
        Rock rock23 = new Rock();
        addObject(rock23,6,6);
        Rock rock24 = new Rock();
        addObject(rock24,1,5);
        Rock rock25 = new Rock();
        addObject(rock25,2,5);
        Rock rock26 = new Rock();
        addObject(rock26,3,5);
        Rock rock27 = new Rock();
        addObject(rock27,4,5);
        Rock rock28 = new Rock();
        addObject(rock28,6,5);
        Rock rock29 = new Rock();
        addObject(rock29,6,4);
        Rock rock30 = new Rock();
        addObject(rock30,4,3);
        Rock rock31 = new Rock();
        addObject(rock31,6,3);
        Rock rock32 = new Rock();
        addObject(rock32,5,3);
        Rock rock33 = new Rock();
        addObject(rock33,3,3);
        Rock rock34 = new Rock();
        addObject(rock34,2,2);
        Rock rock35 = new Rock();
        addObject(rock35,2,1);
        Rock rock36 = new Rock();
        addObject(rock36,2,3);
        removeObject(rock7);
        removeObject(rock);
        Carrot carrot = new Carrot();
        addObject(carrot,1,0);
        
        addObject(new MazeRabbit(), 0, 8);
    }
}
