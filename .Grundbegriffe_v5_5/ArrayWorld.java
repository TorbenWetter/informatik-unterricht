public class ArrayWorld extends WorldRabbitWorld {

    public ArrayWorld() {
        super(13, 13);
        prepare();
    }

    private void prepare() {
        //addObject(new ArrayRabbit(), 1, 1);
        //addObject(new MultiDimArrayRabbit(), 0, 0);
        addObject(new ArrayRabbit2(), 0, 0);
    }
}
