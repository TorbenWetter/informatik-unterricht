

/**
 * Ein ganz normaler Hase.
 * 
 * Benutze die init() Methode um dem Hasen Anweisungen zu geben, die er beim Programmstart ausf�hrt
 * 
 */
public class NormalRabbit extends Rabbit
{
        
    /**
     * Die init() Methode, wird einmalig beim Programmstart aufgerufen.
     * Hier kannst du alle Anweisungen eintragen, die du dem Hasen geben m�chtest.
     */
    public void init(){
        move();
        putCarrot();
        move();
        turnLeft();         
   }
   
}
