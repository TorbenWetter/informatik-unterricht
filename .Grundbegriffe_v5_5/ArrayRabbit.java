import greenfoot.Greenfoot;

import java.util.Arrays;
import java.util.Random;
import java.util.stream.IntStream;

public class ArrayRabbit extends CarefulRabbit {

    public void init() {
        /* AB 10 - Arrays Übungen:
        int[] randomArray = generateRandomArray();

        printArray(randomArray);

        int arraySum = sumArray(randomArray);
        System.out.println("Summe: " + arraySum);

        int[] swappedArray = swapArray(randomArray);
        printArray(swappedArray);

        int[] swappedArray2 = swapArray(randomArray, 5, 7);
        printArray(swappedArray2);

        int[] leftShiftedArray = leftShift(randomArray);
        printArray(leftShiftedArray);

        int[] rightShiftedArray = rightShift(randomArray);
        printArray(rightShiftedArray);

        char[] testCharArray = new char[]{'a', 'b', 'c', 'd', 'e', 'f', 'g'};
        printArray(testCharArray);

        char[] leftShiftedCharArray = leftShift(testCharArray);
        printArray(leftShiftedCharArray);

        char[] rightShiftedCharArray = rightShift(testCharArray);
        printArray(rightShiftedCharArray);

        int[] first100Primes = primes(100);
        printArray(first100Primes);
        */
    }

    private int[] generateRandomArray() {
        final int AMOUNT = 10, START = 0, END = 50;
        return new Random().ints(AMOUNT, START, END).toArray();

        /* ohne Vorkenntnisse:
        int[] array = new int[10];
        for (int i = 0; i < array.length; i++) {
            array[i] = Greenfoot.getRandomNumber(50);
        }
        return array;
        */
    }

    private void printArray(int[] array) {
        System.out.println(Arrays.toString(array));

        /* ohne Vorkenntnisse:
        String arrayString = "[";
        for (int i = 0; i < array.length; i++) {
            if (i != 0) {
                arrayString += ", ";
            }
            arrayString += array[i];
        }
        arrayString += "]";
        System.out.println(arrayString);
        */
    }

    private int sumArray(int[] array) {
        return IntStream.of(array).sum();

        /* ohne Vorkenntnisse:
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }
        return sum;
        */
    }

    private int[] swapArray(int[] array) {
        int temp = array[0];
        array[0] = array[1];
        array[1] = temp;
        return array;
    }

    private int[] swapArray(int[] array, int a, int b) {
        // try catch mit Error in der Konsole schlecht..
        try {
            int temp = array[a];
            array[a] = array[b];
            array[b] = temp;
        } catch (IndexOutOfBoundsException ex) {
            ex.printStackTrace();
        }
        return array;

        /* ohne Vorkenntnisse: (wenn Index nicht in Array, neue Random Nummer generieren)
        int temp = Greenfoot.getRandomNumber(array.length);
        if (0 <= a && a < array.length) {
            temp = array[a];
        }

        if (0 <= b && b < array.length) {
            array[a] = array[b];
        } else {
            array[a] = Greenfoot.getRandomNumber(array.length);
        }

        array[b] = temp;

        return array;
        */
    }

    private int[] leftShift(int[] input) {
        int firstElement = input[0];

        System.arraycopy(input, 1, input, 0, input.length - 1);
        /* ohne Vorkenntnisse:
        for (int i = 1; i < input.length; i++) {
            input[i - 1] = input[i];
        }
        */

        input[input.length - 1] = firstElement;
        return input;
    }

    private int[] rightShift(int[] input) {
        int lastElement = input[input.length - 1];

        System.arraycopy(input, 0, input, 1, input.length - 1);
        /* ohne Vorkenntnisse:
        for (int i = input.length - 1; i > 0; i--) {
            input[i] = input[i - 1];
        }
        */

        input[0] = lastElement;
        return input;
    }

    private void printArray(char[] array) {
        System.out.println(Arrays.toString(array));
    }

    private char[] leftShift(char[] input) {
        char firstElement = input[0];
        System.arraycopy(input, 1, input, 0, input.length - 1);
        input[input.length - 1] = firstElement;
        return input;
    }

    private char[] rightShift(char[] input) {
        char lastElement = input[input.length - 1];
        System.arraycopy(input, 0, input, 1, input.length - 1);
        input[0] = lastElement;
        return input;
    }

    // Fast die gleiche Funktion ist schon in MethodRabbit.. (primeEfficient)
    private int[] primes(int n) {
        int[] primeNumbers = new int[n];
        primeNumbers[0] = 2;

        int primes = 1;
        int possiblePrime = 3;
        while (primes < n) {
            boolean isPrime = true;
            for (int i = 0; i < primes; i++) {
                int prime = primeNumbers[i];

                if (prime > Math.sqrt(possiblePrime)) {
                    break;
                }

                if (possiblePrime % prime == 0) {
                    isPrime = false;
                    break;
                }
            }

            if (isPrime) {
                primes++;
                primeNumbers[primes - 1] = possiblePrime;
            }

            possiblePrime += 2;
        }

        return primeNumbers;
    }
}
