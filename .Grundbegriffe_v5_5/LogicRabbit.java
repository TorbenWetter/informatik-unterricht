import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class LogicRabbit extends CarefulRabbit {

    public void init() {
        /* Aufgabe 2 a)
        while (isCarrotNear() || isRockLeft() && isRockRight()) {
            move();
        }
        // Der Hase läuft so lange, wie
        // - eine Karotte in der Nähe ist ODER
        // - (links UND rechts von ihm ein Fels ist)
        // -> bis Feld 8, weil rechts kein Fels mehr ist
        */

        /* Aufgabe 2 b)
        while ((isCarrotNear() || isRockLeft()) && isRockRight()) {
            move();
        }
        // Der Hase läuft so lange, wie
        // - (eine Karotte in der Nähe ist ODER links von ihm ein Fels ist) UND
        // - rechts von ihm ein Feld ist
        // -> bis Feld 3, weil rechts kein Fels mehr ist
        */

        /* Aufgabe 2 c)
        while ((!isCarrotNear() || isRockLeft()) && canMove()) {
            move();
        }
        // Der Hase läuft so lange, wie
        // - (keine Karotte in der Nähe ist ODER links von ihm ein Fels ist) UND
        // - er sich nach vorne bewegen kann
        // -> bis Feld 3, weil eine Karotte in der Nähe ist UND links von ihm kein Fels ist
        */
    }

    public boolean isRockRight() {
        turnRight();
        boolean rockInFront = !canMove();
        turnLeft();
        return rockInFront;
    }

    public boolean isRockLeft() {
        turnLeft();
        boolean rockInFront = !canMove();
        turnRight();
        return rockInFront;
    }
}
