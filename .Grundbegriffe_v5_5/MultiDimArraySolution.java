public class MultiDimArraySolution {

    public char[][] shiftMultiDimCharArray(char[][] charArray) {
        // Für jedes Array im multidimensionalen Array wird ein rightShift ausgeführt
        for (int i = 0; i < charArray.length; i++) {
            char[] innerArray = charArray[i];

            // "rightShift" wie im Unterricht besprochen
            char lastElement = innerArray[innerArray.length - 1];
            for (int j = innerArray.length - 1; j > 0; j--) {
                innerArray[j] = innerArray[j - 1];
            }
            innerArray[0] = lastElement;

            // Setzen des neuen Arrays
            charArray[i] = innerArray;
        }
        return charArray;
    }
}