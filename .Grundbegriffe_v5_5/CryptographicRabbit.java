public abstract class CryptographicRabbit extends Rabbit {

    /**
     * Generates an array of all upper case ASCII chars
     * Index of first char (A) = 65
     * Index of last char (Z) = 90
     * See https://www.cs.cmu.edu/~pattis/15-1XX/common/handouts/ascii.html for details.
     *
     * @return array of ASCII chars
     */
    public char[] generateUpperCaseAlphabet() {
        return generateAlphabet(65, 90);
    }

    /**
     * Generates an array of all possible (visible) ASCII chars
     * Index of first char (SPACE) = 32
     * Index of last char (~) = 126
     * See https://www.cs.cmu.edu/~pattis/15-1XX/common/handouts/ascii.html for details.
     *
     * @return array of ASCII chars
     */
    public char[] generateAlphabet() {
        return generateAlphabet(32, 126);
    }

    /**
     * Generates an array of all possible ASCII chars with an int value
     * between firstCharValue und lastCharValue
     *
     * @param firstCharValue int value of the first char of the alphabet
     * @param lastCharValue  int value of the last char of the alphabet
     * @return alphabet
     */
    private char[] generateAlphabet(int firstCharValue, int lastCharValue) {

        char[] alphabet = new char[lastCharValue - firstCharValue + 1];

        for (int i = 0; i < alphabet.length; i++) {
            alphabet[i] = (char) (i + firstCharValue);
        }

        return alphabet;
    }

    /**
     * Prints all chars in the array in a single line
     *
     * @param array
     */
    public void print(char[] array) {
        //System.out.print("#");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i]);
        }
        System.out.println();
    }

    /**
     * Checks if the text contains the snippet
     *
     * @param text
     * @param snippet
     * @return
     */
    public boolean contains(char[] text, char[] snippet) {
        return new String(text).contains(new String(snippet));
    }
}