import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class WorldAB_CarrotStackRow extends WorldRabbitWorld {

    public WorldAB_CarrotStackRow() {
        super(15, 10);
        prepare();
    }

    public void prepare() {
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 10; j++) {
                addObject(new RandomCarrot(0.5), i + 3, 5);
            }
        }
        /* 06 Aufgabe 1
        addObject(new CarrotRabbit1(), 0, 5);
        */
        /* AB 08 - Methoden Einstieg und Aufgaben
        addObject(new MethodRabbit(), 0, 5);
        */
    }
}
