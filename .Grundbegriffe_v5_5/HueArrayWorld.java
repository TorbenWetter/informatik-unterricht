import greenfoot.World;

public class HueArrayWorld extends WorldRabbitWorld {

    public HueArrayWorld() {
        super();
        prepare();
    }

    private void prepare() {
        addObject(new HueArrayRabbit(), 0, 0);
    }
}
