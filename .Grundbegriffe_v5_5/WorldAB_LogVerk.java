import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class WorldAB_LogVerk extends WorldRabbitWorld {

    public WorldAB_LogVerk() {
        super(15, 10);
        prepare();
    }

    private void prepare() {
        Rock rock = new Rock();
        addObject(rock, 1, 3);
        Rock rock2 = new Rock();
        addObject(rock2, 1, 5);
        Rock rock3 = new Rock();
        addObject(rock3, 2, 3);
        Rock rock4 = new Rock();
        addObject(rock4, 2, 5);
        Rock rock5 = new Rock();
        addObject(rock5, 5, 3);
        removeObject(rock5);
        Rock rock6 = new Rock();
        addObject(rock6, 4, 3);
        Rock rock7 = new Rock();
        addObject(rock7, 4, 5);
        Rock rock8 = new Rock();
        addObject(rock8, 6, 3);
        Rock rock9 = new Rock();
        addObject(rock9, 6, 5);
        Rock rock10 = new Rock();
        addObject(rock10, 8, 5);
        removeObject(rock10);
        Rock rock11 = new Rock();
        addObject(rock11, 7, 5);
        Rock rock12 = new Rock();
        addObject(rock12, 9, 5);
        Rock rock13 = new Rock();
        addObject(rock13, 7, 3);
        Rock rock14 = new Rock();
        addObject(rock14, 8, 3);
        Rock rock15 = new Rock();
        addObject(rock15, 9, 3);
        Carrot carrot = new Carrot();
        addObject(carrot, 1, 4);
        Carrot carrot2 = new Carrot();
        addObject(carrot2, 2, 4);
        Carrot carrot3 = new Carrot();
        addObject(carrot3, 3, 4);
        Carrot carrot4 = new Carrot();
        addObject(carrot4, 5, 4);
        LogicRabbit logicrabbit = new LogicRabbit();
        addObject(logicrabbit, 1, 4);
    }
}
