

/**
 * Write a description of class ZufallsStein here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class RandomRock extends Rock
{
   private double propability = 1.0d;
    
   private boolean initDone = false;
    
    public RandomRock(double propability){
        this.propability = propability;       
    }
    
    public void act(){
         if(!initDone){
            init();
            initDone = true;
        }
    }
    
    public void init(){
        if(propability > Math.random()){
            getWorld().removeObject(this);
        }
    }
}
