import java.util.Arrays;

public class MethodRabbit extends CarefulRabbit {

    @Override
    public void init() {
        /* AB 08 - Methoden Einstieg und Aufgaben -> 0 - 4
        moveN(5);
        putCarrot(3);
        putCarrotRow(6);
        System.out.println(countCarrots());
        moveSteps(2, 3);
        moveTo(6, 4);
        System.out.println(countStepsTo(3, 3));
        */

        /*
        -> Aufgabe 5
        a) Parameter: (int from, int to), Rückgabetyp: int
        b) Die Methode addiert die Zahlen zwischen from (inkludiert) und to (exkludiert) und gibt die Summe zurück
        c) (1, 5) -> 10 [1 + 2 + 3 + 4]
        d) rangeSum()
        */

        /* Aufgabe 6
        b) Neben dem Namen (von Methode und Variablen) muss außerdem die Startzahl von 0 auf 1 geändert werden
        printEven(10);
        printUneven(13);
        */

        /* Aufgabe 7
        Zusatz: unendlich große Anzahl an Werten, die übergeben werden können
        System.out.println(maximum(3.5, 6.1, 9.6));
        */

        /* Aufgabe 8
        prime(1000000);
        */
    }

    public int amountToLimit(int amount) {
        int limit = amount;
        while ((limit / Math.log(limit)) - amount < 0) {
            limit++;
        }
        return limit;
    }

    // Abgeänderter Code von https://www.geeksforgeeks.org/sieve-of-atkin/
    public void prime(int amount) {
        long startTime = System.currentTimeMillis();

        int limit = amountToLimit(amount);

        // Erstelle ein "limit"-großes boolean-Array nur mit false-Werten
        boolean[] sieve = new boolean[limit + 1];
        Arrays.fill(sieve, false);

        // 2 und 3 sind auf jeden Fall Primzahlen
        sieve[2] = true;
        sieve[3] = true;

        // Loope durch x von 1 bis x^2 < limit und y von 1 bis y^2 < limit
        // Wenn eine der drei Voraussetzungen wahr ist, wird die passende Stelle im sieve-Array auf true gesetzt
        for (int x = 1; x * x < limit; x++) {
            for (int y = 1; y * y < limit; y++) {
                /*
                a) n = (4 * x * x) + (y * y) hat eine ungerade Anzahl von Lösungen, d.h. es gibt eine ungerade Anzahl
                       von verschiedenen Paaren (x, y), die die Gleichung erfüllen
                       UND
                   n % 12 = 1 or n % 12 = 5
                */
                int n = (4 * x * x) + (y * y);
                if (n <= limit && (n % 12 == 1 || n % 12 == 5)) {
                    sieve[n] ^= true;
                }

                /*
                b) n = (3 * x * x) + (y * y) hat eine ungerade Anzahl von Lösungen
                       UND
                   n % 12 = 7
                */
                n = (3 * x * x) + (y * y);
                if (n <= limit && n % 12 == 7) {
                    sieve[n] ^= true;
                }

                /*
                c) n = (3 * x * x) - (y * y) hat eine ungerade Anzahl von Lösungen
                       UND
                   x > y
                       UND
                   n % 12 = 11
                */
                n = (3 * x * x) - (y * y);
                if (x > y && n <= limit && n % 12 == 11) {
                    sieve[n] ^= true;
                }
            }
        }

        // Markiere alle Vielfachen von Quadraten als Nicht-Primzahl
        for (int r = 5; r * r < limit; r++) {
            if (sieve[r]) {
                for (int i = r * r; i < limit; i += r * r) {
                    sieve[i] = false;
                }
            }
        }

        // Gibt alle Primzahlen aus (ab Index 2, weil Indices 0 und 1 egal sind)
        int primesPrinted = 0;
        for (int a = 2; a < limit; a++) {
            if (primesPrinted < amount) {
                if (sieve[a]) {
                    System.out.println(a);
                    primesPrinted++;
                }
            } else {
                break;
            }
        }

        long endTime = System.currentTimeMillis();
        System.out.println("Die Berechnung von " + primesPrinted + " Primzahlen hat " + (endTime - startTime) + " Millisekunden gedauert.");
    }

    public void primeEfficient(int amount) {
        long startTime = System.currentTimeMillis();

        int[] primeNumbers = new int[amount];
        primeNumbers[0] = 2;

        int primes = 1;
        int possiblePrime = 3;
        while (primes < amount) {
            boolean isPrime = true;
            for (int i = 0; i < primes; i++) {
                int prime = primeNumbers[i];

                if (prime > Math.sqrt(possiblePrime)) {
                    break;
                }

                if (possiblePrime % prime == 0) {
                    isPrime = false;
                    break;
                }
            }

            if (isPrime) {
                System.out.println(possiblePrime);
                primes++;
                primeNumbers[primes - 1] = possiblePrime;
            }

            possiblePrime += 2;
        }

        long endTime = System.currentTimeMillis();
        System.out.println("Die Berechnung von " + amount + " Primzahlen hat " + (endTime - startTime) + " Millisekunden gedauert.");
    }

    public void primeInefficient(int amount) {
        long startTime = System.currentTimeMillis();
        int primesFound = 0;
        int possiblePrime = 2;
        while (primesFound < amount) {
            boolean isPrime = true;
            for (int i = 2; i < possiblePrime; i++) {
                if (possiblePrime % i == 0) {
                    isPrime = false;
                }
            }
            if (isPrime) {
                primesFound++;
                System.out.println(possiblePrime);
            }
            possiblePrime++;
        }
        long endTime = System.currentTimeMillis();
        System.out.println("Die Berechnung von " + amount + " Primzahlen hat " + (endTime - startTime) + " Millisekunden gedauert.");
    }

    public double maximum(double... n) {
        if (n.length == 0) {
            return -1;
        }

        double maximum = n[0];
        for (int i = 1; i < n.length; i++) {
            if (n[i] > maximum) {
                maximum = n[i];
            }
        }
        return maximum;
    }

    public void printEven(int to) {
        int evenNumber = 0;
        while (evenNumber < to) { // Der Fehler war, dass > statt < benutzt wurde (es muss aufwärts gezählt werden)
            evenNumber += 2; // Fehler: evenNumber wurde bisher nur immer auf 2 gesetzt, es soll aber 2 addiert werden
            System.out.println(evenNumber); // Gibt den Inhalt von evenNumber aus
        }
    }

    public void printUneven(int to) {
        int unevenNumber = 1;
        while (unevenNumber < to) {
            unevenNumber += 2;
            System.out.println(unevenNumber); // Gibt den Inhalt von unevenNumber aus
        }
    }

    /**
     * 1) Überlege was diese Methode macht.
     * 2) Finde einen sinnvollen Namen für die Methode und die darin verwendete Variable.
     * 3) Gib ein Beispielaufruf an mit dem die Methode aufgerufen werden kann.
     * 4) Überprüfe deine Überlegung indem du die Methode in einen deiner Hasen integrierst und
     * die Methode mit unterschiedlichen Werten aufrufst.
     * Antworten:
     * 1) Führt n mal move() aus (Abzählen mit Hilfe einer while-Schleife und Verringern um 1 in dieser)
     * 2) moveN(int n) {}
     * 3) moveN(5);
     * 4) Gemacht.
     */
    private void moveN(int n) {
        while (n > 0) {
            move();
            n = n - 1;
        }
    }

    private void putCarrot(int n) {
        for (int i = 0; i < n; i++) {
            putCarrot();
        }
    }

    private void putCarrotRow(int length) {
        for (int i = 0; i < length; i++) {
            putCarrot();
            move();
        }
    }

    private int countCarrots() {
        int carrotsPicked = 0;
        while (isCarrotNear()) {
            pickCarrot();
            carrotsPicked++;
        }
        putCarrot(carrotsPicked);
        return carrotsPicked;
    }

    private void moveSteps(int x, int y) {
        int START_ROTATION = getRotation();
        setRotation(RIGHT);
        if (x < 0) {
            setRotation(LEFT);
            x = -x;
        }
        moveN(x);

        setRotation(DOWN);
        if (y < 0) {
            setRotation(UP);
            y = -y;
        }
        moveN(y);
        setRotation(START_ROTATION);
    }

    private void moveTo(int x, int y) {
        moveSteps(x - getX(), y - getY());
    }

    private int countStepsTo(int x, int y) {
        return Math.abs(getX() - x) + Math.abs(getY() - y);
    }
}