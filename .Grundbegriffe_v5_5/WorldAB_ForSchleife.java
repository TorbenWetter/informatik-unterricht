public class WorldAB_ForSchleife extends WorldRabbitWorld {

    public WorldAB_ForSchleife() {
        super(13, 13);
        prepare();
    }

    private void prepare() {
        RabbitAB_ForSchleife rabbitab_forschleife = new RabbitAB_ForSchleife();
        addObject(rabbitab_forschleife, 1, 10);
    }
}
