import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class BinaryDecimalRabbit extends CarefulRabbit {

    public void init() {
        /* 06 Aufgabe 2
        turnAround();
        int binaryNumber = 0, binaryPower = -1;
        while (canMove()) {
            move();
            binaryPower++;
            if (isRockAbove() && isCarrotNear()) {
                binaryNumber += Math.pow(2, binaryPower);
            }
        }
        turnAround();
        for (int i = 0; i < binaryPower; i++)
            move();
        turnAround();
        System.out.println(binaryNumber);
        */
    }

    private boolean isRockAbove() {
        turnRight();
        boolean canMove = canMove();
        turnLeft();
        return !canMove;
    }
}
