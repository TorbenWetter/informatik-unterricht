import java.util.concurrent.TimeUnit;

public class MultiDimArrayRabbit extends CarefulRabbit {

    @Override
    public void init() {
        // Ein "Auto", bestehend aus einem multi(zwei)dimensionalen char-Array
        char[][] car = new char[][]{
                {' ', ' ', ' ', '-', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '_', '_', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},
                {' ', '-', '-', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '~', '(', ' ', '@', '\\', ' ', ' ', ' ', '\\', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},
                {'-', '-', '-', ' ', ' ', ' ', '_', '_', '_', '_', '_', '_', '_', '_', '_', ']', '_', '[', '_', '_', '/', '_', '>', '_', '_', '_', '_', '_', '_', '_', '_', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},
                {' ', ' ', ' ', ' ', ' ', '/', ' ', ' ', '_', '_', '_', '_', ' ', '\\', ' ', '<', '>', ' ', ' ', ' ', ' ', ' ', '|', ' ', ' ', '_', '_', '_', '_', ' ', ' ', '\\', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},
                {' ', ' ', ' ', ' ', '=', '\\', '_', '/', ' ', '_', '_', ' ', '\\', '_', '\\', '_', '_', '_', '_', '_', '_', '_', '|', '_', '/', ' ', '_', '_', ' ', '\\', '_', '_', 'D', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},
                {'_', '_', '_', '_', '_', '_', '_', '_', '(', '_', '_', ')', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '(', '_', '_', ')', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_', '_'}};

        // 20 Mal soll das "Auto" je einen Charakter nach rechts verschoben werden
        for (int i = 0; i < 20; i++) {
            // Vor jedem Verschieben wird eine halbe Sekunde lang (500 Millisekunden) gewartet
            // -> Ausführung 2x/Sekunde
            try {
                TimeUnit.MILLISECONDS.sleep(500);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
            // Das einen Charakter nach rechts verschobene Auto wird in die Variable geschrieben
            car = shiftMultiDimCharArray(car);
            // Das neue "Auto" wird ausgegeben
            printMultiDimCharArray(car);
        }
    }

    public char[][] shiftMultiDimCharArray(char[][] charArray) {
        // Schreibe hier Deinen Code hinein, der das "Auto" einen Charakter nach rechts bewegt

        return charArray;
    }

    public void printMultiDimCharArray(char[][] charArray) {
        // Gibt 100 leere Zeilen aus
        for (int i = 0; i < 100; i++) {
            System.out.println();
        }
        // Gibt jedes einzelne char-Array zeilenweise als String aus
        for (int i = 0; i < charArray.length; i++) {
            char[] lineArray = charArray[i];
            System.out.println(String.valueOf(lineArray));
        }
    }
}