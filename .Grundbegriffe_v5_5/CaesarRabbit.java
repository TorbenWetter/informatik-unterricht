public class CaesarRabbit extends CryptographicRabbit {

    /**
     * Aufgabe 1) Informiere dich auf inf-schule über die Caesar Chiffre:
     * https://www.inf-schule.de/programmierung/imperativeprogrammierung/fallstudien/modularisierung/fallstudie_verschluesselungcaesar/automatisierung
     * <p>
     * Aufgabe 2) Implementiere die Methode getOrdinalNumber
     *
     * @see #getOrdinalNumber(char[], char)
     * <p>
     * Aufgabe 3) Implementiere die Caesar Chiffre in der Methode applyChiffre(char[] input, char[] alphabet, int secret)
     * @see #applyChiffre(char[], char[], int)
     * <p>
     * Aufgabe 4) Verschlüssele einen Text und gib den Text deinem Nachbarn zum Entschlüsseln.
     * Teile ihm das Geheimnis mit, das Du zum Verschlüsseln verwendet hast.
     * Verwende hierzu das Alphabet, das von der Methode getUpperCaseAlphabet() erzeugt wird.
     * WICHTIG: Es dürfen nur Großbuchstaben verwendet werden.
     * Es dürfen im Klartext keine Leerzeichen, Umlaute, Kleinbuchstaben oder sonstige Sonderzeichen vorkommen.
     * @see #undoChiffre(char[], char[], int)
     * <p>
     * Aufgabe 5) Verschlüssele einen Text und gib den Text deinem Nachbarn zum Entschlüsseln.
     * Diesmal bekommt Dein Nachbar kein Geheimnis übergeben.
     * Teile deinem Nachbarn einen kurzen Textbaustein (>3 Zeichen) mit, der im Klartext vorkommt.
     * Verwende hierzu das Alphabet, das von der Methode getUpperCaseAlphabet() erzeugt wird.
     * WICHTIG: Es dürfen nur Großbuchstaben verwendeten werden.
     * Es dürfen im Klartext keine Leerzeichen, Umlaute, Kleinbuchstaben oder sonstige Sonderzeichen vorkommen.
     * @see #undoChiffre(char[], char[], char[])
     * <p>
     * Aufgabe 5) Entschlüssele die Nachricht deines Nachbarn.
     * @see #undoChiffre(char[], char[], char[])
     * <p>
     * Aufgabe 6) Schreibe ein Programm, das dir automatisch den Klartext erzeugt ohne, dass du das Geheimnis kennst.
     * @see #undoChiffre(char[], char[], char[])
     * <p>
     * Aufgabe 7) Entschlüssele die geheime Nachricht von Herr Schaede:
     * l!#(%?%).?e/4/?6/.?$)%3%-?s%84?5.$?3%.$%?%3?!.?*M3#(!%$%_'9-.!3)5-L/"%234!$4M$%@
     * Tipp: Herr Schaede hat das Alphabet verwendet, dass von der Methode getAlphabet() erzeugt wurde.
     * <p>
     * Der folgende Text kommt in der ursprünglichen Nachricht vor:
     * ober
     * oder als array
     * new char[]{'o','b','e','r'}
     * <p>
     * Hilfsmittel:
     * Klasse CryptographicRabbit
     * - getUpperCaseAlphabet() => liefert ein Array von allen Großbuchstaben [A-Z]
     * - getAlphabet() => liefert ein Array von allen sichtbaren ASCII Zeichen [a-zA-Z0-9 und Sonderzeichen]
     * - print(char[] toPrint) => Gibt das übergebene char[] array auf der Konsole aus
     * - contains(char[] text, char[] snippet) => liefert ein boolschen Wert der angibt ob das snippet im text enthalten ist.
     * Beispiel: text={'H', 'a', 'l', 'l', 'o'}, snippet={'a', 'l', 'l'} => true
     * text={'H', 'a', 'l', 'l', 'o'}, snippet={'l', 'a'} => false
     **/

    @Override
    public void init() {
        char[] upperCaseAlphabet = super.generateUpperCaseAlphabet();
        print(upperCaseAlphabet);

        // Aufgabe 3)
        char[] cryptic = "SALVEASTERIX".toCharArray();
        print(cryptic);

        int secret = 3;

        char[] encrypted = applyChiffre(upperCaseAlphabet, cryptic, secret);
        print(encrypted);

        // Aufgabe 4)
        char[] decrypted = undoChiffre(upperCaseAlphabet, encrypted, secret);
        print(decrypted);

        // Aufgabe 5)
        String crypticString2 = "TESTNACHRICHT";
        char[] cryptic2 = crypticString2.toCharArray();
        print(cryptic2);

        int secret2 = 5;

        char[] encrypted2 = applyChiffre(upperCaseAlphabet, cryptic2, secret2);
        print(encrypted2);

        char[] plainPart2 = crypticString2.substring(3, 7).toCharArray();
        print(plainPart2);

        // Aufgabe 6)
        char[] decrypted2 = undoChiffre(upperCaseAlphabet, encrypted2, plainPart2);
        print(decrypted2);

        // Aufgabe 7)
        char[] alphabet = super.generateAlphabet();
        print(alphabet);

        char[] encrypted3 = "l!#(%?%).?e/4/?6/.?$)%3%-?s%84?5.$?3%.$%?%3?!.?*M3#(!%$%_'9-.!3)5-L/\"%234!$4M$%@".toCharArray();
        print(encrypted3);

        char[] plainPart3 = new char[]{'o', 'b', 'e', 'r'};
        print(plainPart3);

        char[] decrypted3 = undoChiffre(alphabet, encrypted3, plainPart3);
        print(decrypted3);
    }

    /**
     * Applys the caesar chiffre to the input array
     *
     * @param input  input text
     * @param secret used to encrypt/decrypt the input
     * @return encrypted/decrypted array
     */
    public char[] applyChiffre(char[] alphabet, char[] input, int secret) {
        for (int i = 0; i < input.length; i++) {
            char c = input[i];
            int index = getOrdinalNumber(alphabet, c);
            int newIndex = Math.floorMod(index + secret, alphabet.length);
            input[i] = alphabet[newIndex];
        }
        return input;
    }

    public char[] undoChiffre(char[] alphabet, char[] input, int secret) {
        return applyChiffre(alphabet, input, -secret);
    }

    public char[] undoChiffre(char[] alphabet, char[] input, char[] plainPart) {
        for (int i = 1; i < alphabet.length; i++) {
            char[] decrypted = applyChiffre(alphabet, input.clone(), -i);
            if (contains(decrypted, plainPart)) {
                return decrypted;
            }
        }
        return input;
    }

    /**
     * Searches for the character in the alphabet array and returns the index (ordinal number) of the
     * slot where the character has been found.
     *
     * @param character that needs to be found in the alphabet
     * @return index of the character in the alphabet
     */
    public int getOrdinalNumber(char[] alphabet, char character) {
        for (int i = 0; i < alphabet.length; i++) {
            if (alphabet[i] == character) {
                return i;
            }
        }
        return -1;
    }
}