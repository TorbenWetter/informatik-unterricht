import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class RabbitAB_ForSchleife extends CarefulRabbit {

    public void init() {
        /* AB_forSchleife | Aufgabe 2 d):
        Teil 1: Initialisierung der laufenden Variable (Zähler)
        Teil 2: Bedingung (falls sie nicht mehr wahr ist, wird die Schleife abgebrochen)
        Teil 3: Verändern der laufenden Variable (damit die Bedingung irgendwann nicht mehr wahr ist)

        for (int i = 0; i < 10; i++) {
            putCarrot();
            move();
        }
        */

        /* Aufgabe 2 e):
        int i = 0;
        while (i < 10) {
            putCarrot();
            move();
            i++;
        }
        */

        /* Aufgabe 2 f):
        Die for-Schleife braucht eindeutig weniger Platz und kann in einer Zeile geschrieben werden, während die
        while-Schleife mindestens drei Zeilen beansprucht.
        Die for-Schleife eignet sich für (eher simpleres) mehrfaches Ausführen eines bestimmten Codes,
        die while-Schleife kann mit komplizierteren Bedingungen genutzt werden und ist somit in mehr Fällen nutzbar
        */

        /* Aufgabe 3:
        putCarrot();
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 10 - 1; j++) {
                move();
                putCarrot();
            }
            turnLeft();
        }
        */

        /* Aufgabe 4:
         */
        carrotSquare(6);

        /* Aufgabe 5:
        System.out.println(carrotSquareArea(8));
        */
    }

    private void carrotSquare(int n) {
        putCarrot();
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < n - 1; j++) {
                move();
                putCarrot();
            }
            turnLeft();
        }
    }

    private int carrotSquareArea(int n) {
        carrotSquare(n);
        return (int) Math.pow(n - 2, 2);
    }
}
