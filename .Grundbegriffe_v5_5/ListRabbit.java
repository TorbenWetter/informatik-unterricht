import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class ListRabbit extends CarefulRabbit {

    @Override
    public void init() {
        List<Integer> sorted1 = sortedList(5);
        System.out.println(sorted1);

        int elementToAdd = 24;
        System.out.println(elementToAdd);
        List<Integer> added = addElementToSortedList(sorted1, elementToAdd);
        System.out.println(added);

        List<Integer> sorted2 = sortedList(6);
        System.out.println(sorted2);

        List<Integer> merged = mergeLists(added, sorted2);
        System.out.println(merged);

    }


    public List<Integer> addElementToSortedList(List<Integer> sorted, int toAdd) {
        sorted = new LinkedList<>(sorted);
        if (toAdd > ((LinkedList<Integer>) sorted).getLast()) {
            sorted.add(toAdd);
            return sorted;
        }
        for (int i = 0; i < sorted.size(); i++) {
            if (toAdd < sorted.get(i)) {
                sorted.add(i, toAdd);
                return sorted;
            }
        }
        return sorted;
    }

    public List<Integer> mergeLists(List<Integer> sorted1, List<Integer> sorted2) {
        sorted1 = new LinkedList<>(sorted1);
        for (int i = 0; i < sorted2.size(); i++) {
            sorted1 = addElementToSortedList(sorted1, sorted2.get(i));
        }
        return sorted1;
    }

    public List<Integer> mergeLists2(List<Integer> list1, List<Integer> list2) {
        for (int i = 0; i < list1.size(); i++) {
            int toAdd = list1.get(i);

            int size = list2.size();
            for (int j = 0; j < size; j++) {
                int element = list2.get(j);
                if (toAdd < element) {
                    list2.add(j, toAdd);
                    break;
                }
            }
            if (toAdd > list2.get(list2.size() - 1)) {
                list2.add(toAdd);
            }
        }
        return list2;
    }

    public List<Integer> sortedList(int size) {
        List<Integer> sorted = new LinkedList<>();
        int randomNumber = 0;
        for (int i = 0; i < size; i++) {
            randomNumber += new Random().nextInt(15);
            sorted.add(randomNumber);
        }
        return sorted;
    }
}
