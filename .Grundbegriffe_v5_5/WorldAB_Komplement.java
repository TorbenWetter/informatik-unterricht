import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class WorldAB_Komplement extends WorldRabbitWorld {

    public WorldAB_Komplement() {
        super(12, 6);
        prepare();
    }

    private void prepare() {
        RandomRock rock1 = new RandomRock(0.3);
        addObject(rock1, 1, 2);
        Rock rock2 = new Rock();
        addObject(rock2, 2, 2);
        Rock rock3 = new Rock();
        addObject(rock3, 3, 2);
        Rock rock4 = new Rock();
        addObject(rock4, 4, 2);
        Rock rock5 = new Rock();
        addObject(rock5, 5, 2);
        Rock rock6 = new Rock();
        addObject(rock6, 6, 2);
        Rock rock7 = new Rock();
        addObject(rock7, 7, 2);
        Rock rock8 = new Rock();
        addObject(rock8, 8, 2);
        Rock rock9 = new Rock();
        addObject(rock9, 9, 2);


        Carrot carrot = new Carrot();
        addObject(carrot, 9, 3);
        Carrot carrot2 = new Carrot();
        addObject(carrot2, 8, 3);
        Carrot carrot3 = new Carrot();
        addObject(carrot3, 5, 3);
        Carrot carrot4 = new Carrot();
        addObject(carrot4, 2, 3);

        /* 05d Aufgabe 2 a)
        addObject(new BinaryRabbit(), 10, 3);
        */

        /* 06 Aufgabe 2
        addObject(new BinaryDecimalRabbit(), 10, 3);
        */

        /* 06 Aufgabe 3
        for (int i = 0; i < 9; i++) {
            addObject(new RandomCarrot(0.5), i + 1, 4);
        }

        addObject(new BinaryAddRabbit(), 10, 3);
        */
    }
}
