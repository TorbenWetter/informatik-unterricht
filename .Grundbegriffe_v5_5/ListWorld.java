public class ListWorld extends WorldRabbitWorld {

    public ListWorld() {
        super(13, 13);
        prepare();
    }

    private void prepare() {
        addObject(new ListRabbit(), 0, 0);
    }
}
