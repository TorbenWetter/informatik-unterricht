import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class BinaryAddRabbit extends CarefulRabbit {

    public void init() {
        /* 06 Aufgabe 3
        turnAround();
        boolean carryOver = false;
        int fieldsMoved = 0;
        while (canMove()) {
            move();
            fieldsMoved++;
            if (isRockAbove()) {
                carryOver = addBinaryAndReturnCarryOver(carryOver);
            } else {
                if (carryOver) {
                    turnLeft();
                    move(2);
                    putCarrot();
                    turnRight();
                    move(2);
                    turnLeft();
                }
            }
        }
        turnAround();
        for (int i = 0; i < fieldsMoved; i++)
            move();
        turnAround();
        */
    }

    private boolean isRockAbove() {
        turnRight();
        boolean canMove = canMove();
        turnLeft();
        return !canMove;
    }

    private boolean addBinaryAndReturnCarryOver(boolean carryOver) {
        turnLeft();
        int carrotsInColumn = 0;
        if (isCarrotNear()) {
            carrotsInColumn++;
        }
        move();
        if (isCarrotNear()) {
            carrotsInColumn++;
        }
        if (carryOver) {
            carrotsInColumn++;
        }
        move();
        if (carrotsInColumn % 2 == 1) {
            putCarrot();
        }
        turnAround();
        move(2);
        turnLeft();
        return carrotsInColumn > 1;
    }
}
