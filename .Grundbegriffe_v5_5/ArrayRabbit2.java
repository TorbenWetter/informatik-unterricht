import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArrayRabbit2 extends CarefulRabbit {

    public void init() {
        // Arrays Übungen 2

        /* Aufgabe 1 a):
        boolean[] toPaint = {false, false, true, true, true, false, false, true};
        paint(toPaint);
        */

        /* Aufgabe 1 b):
        boolean[][] toPaintMulti = {
                {false, false, true, true, true, false, false, true},
                {true, true, false, false, false, false, true, true},
                {false, true, false, false, false, true, true, true}
        };
        paint(toPaintMulti);
        */

        /* Aufgabe 2 a):
        int[][] worldArray = worldToArray();
        System.out.println(Arrays.deepToString(worldArray));
        */

        /* Aufgabe 2 b):
        boolean[][] boolWorldArray = intToBooleanArray(worldArray);
        System.out.println(Arrays.deepToString(boolWorldArray));
        */

        /* Aufgabe 3 a):
        boolean[] binaryArray = new boolean[]{true, false, true, true};
        boolean[] doubledBinaryArray = binaryTimesTwo(binaryArray);
        System.out.println(Arrays.toString(doubledBinaryArray));
        */

        /* Aufgabe 3 b):
        boolean[] binaryArray1 = new boolean[]{false, true, true, false, false};
        boolean[] binaryArray2 = new boolean[]{true, false, true, true, true};
        boolean[] addedBinaryArray = addBinary(binaryArray1, binaryArray2);
        System.out.println(Arrays.toString(addedBinaryArray));
        */
    }

    public void paint(boolean[] toPaint) {
        for (int i = 0; i < toPaint.length; i++) {
            if (toPaint[i]) {
                putCarrot();
            }
            if (i < toPaint.length - 1) {
                move();
            }
        }
        turnAround();
        move(toPaint.length - 1);
        turnAround();
    }

    public void paint(boolean[][] toPaint) {
        for (int i = 0; i < toPaint.length; i++) {
            boolean[] innerToPaint = toPaint[i];
            paint(innerToPaint);
            if (i < toPaint.length - 1) {
                turnRight();
                move();
                turnLeft();
            }
        }
    }

    public int[][] worldToArray() {
        int[][] array = new int[getWorld().getHeight()][getWorld().getWidth()];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                int carrotAmount = 0;
                while (isCarrotNear()) {
                    pickCarrot();
                    carrotAmount++;
                }
                for (int k = 0; k < carrotAmount; k++) {
                    putCarrot();
                }
                array[i][j] = carrotAmount;
                if (j < array[i].length - 1) {
                    move();
                }
            }
            turnAround();
            move(array.length - 1);
            turnAround();
            if (i < array.length - 1) {
                turnRight();
                move();
                turnLeft();
            }
        }
        return array;
    }

    public boolean[][] intToBooleanArray(int[][] array) {
        if (array.length == 0) {
            return new boolean[0][0];
        }
        boolean[][] newArray = new boolean[array.length][array[0].length];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                newArray[i][j] = array[i][j] > 0;
            }
        }
        return newArray;
    }

    public boolean[] binaryTimesTwo(boolean[] input) {
        boolean[] newArray = new boolean[input.length + 1];
        for (int i = 0; i < input.length; i++) {
            newArray[i] = input[i];
        }
        newArray[newArray.length - 1] = false;
        return newArray;
    }

    public boolean[] addBinary(boolean[] input1, boolean[] input2) {
        if (input1.length != input2.length) {
            return new boolean[0];
        }

        boolean[] newArray = new boolean[input1.length + 1];
        boolean carryover = false;
        for (int i = input1.length - 1; i >= 0; i--) {
            boolean[] addArray = new boolean[]{input1[i], input2[i], carryover};
            int trueAmount = 0;
            for (int j = 0; j < addArray.length; j++) {
                if (addArray[j]) {
                    trueAmount += 1;
                }
            }

            newArray[i + 1] = trueAmount % 2 != 0;
            carryover = trueAmount > 1;
        }
        newArray[0] = carryover;
        return newArray;
    }
}
