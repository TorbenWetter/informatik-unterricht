
import java.util.List;

// imports Actor, World, Greenfoot, GreenfootImage
import greenfoot.Actor;
import greenfoot.GreenfootImage;
import greenfoot.World;
import greenfoot.Greenfoot;

/**
 * Die Mutter aller Hasen
 */
public abstract class Rabbit extends Actor {

    protected int direction = 0; // aktuelle Richtung
    protected int carrotCount;

    private GreenfootImage imgRight;
    private GreenfootImage imgLeft;

    private boolean initDone = false;

    /**
     * Erzeugt einen Hasen mit 5000 Karotten
     */
    public Rabbit() {
        this(5000);

    }

    /**
     * Erzeuge einen Hasen
     * 
     * @param carrotCount
     *            Anzahl der Karotten die der Hase von Anfang an bei sich tr�gt.
     */
    public Rabbit(int carrotCount) {
        imgRight = getImage();
        imgLeft = new GreenfootImage(getImage());
        imgLeft.mirrorVertically();

        this.carrotCount = carrotCount;

    }

    /**
     * Nehme eine Karotte auf
     */
    public void pickCarrot() {
        sleep();
        Actor karotte = getOneObjectAtOffset(0, 0, Carrot.class);
        if (karotte != null) {
            getWorld().removeObject(karotte);
            carrotCount = carrotCount + 1;
        } else {
            ((WorldRabbitWorld) getWorld()).alert("Take a carrot ... ?!? There is none here.");
            throw new RuntimeException(this.toString() + " tries to take a carrot at (" + this.getX() + ", "
                    + this.getY() + ") but there is none here!");
        }
    }

    /**
     * Lege eine Karotte ab
     */
    public void putCarrot() {
        sleep();
        if (carrotCount > 0) {
            getWorld().addObject(new Carrot(), this.getX(), this.getY());

            carrotCount = carrotCount - 1;
        } else {
            ((WorldRabbitWorld) getWorld()).alert("Put a carrot ... ?!? There are none left to put");
            throw new RuntimeException(this + " has no more carrots to put!");
        }
    }

    /**
     * Bewege Dich einen Schritt nach vorne falls m�glich. Sonst tue nichts.
     */
    public void move() {
        sleep();
        int oldX = getX();
        int oldY = getY();
        super.move(1);

        if (!validField(this.getX(), this.getY())) {
            ((WorldRabbitWorld) getWorld()).alert("That hurts!");
            String msg = this + " wanted to move from (" + oldX + ", " + oldY + ") to (" + this.getX() + ", "
                    + this.getY() + "), but something is blocking the way.";

            throw new RuntimeException(msg);
        }else if (oldX==getX() && oldY==getY()) {
            ((WorldRabbitWorld) getWorld()).alert("Suicide is no solution!");
            String msg = this + " tried to leave the world at (" + oldX + ", " + oldY + ").";

            throw new RuntimeException(msg);
        }

    }

    protected void sleep() {
        WorldRabbitWorld.sleep();
    }

    protected boolean validField(int x, int y) {
        World myWorld = getWorld();
        if (x >= myWorld.getWidth() || y >= myWorld.getHeight()) {
            return false;
        } else if (x < 0 || y < 0) {
            return false;
        }

        List<Rock> steine = myWorld.getObjectsAt(x, y, Rock.class);
        if (steine.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Nach links drehen.
     */
    public void turnLeft() {
        sleep();

        setRotation(direction - 90);
        direction = getRotation();
        if (getRotation() == 0) {
            setImage(imgRight);
        } else if (getRotation() == 180) {
            setImage(imgLeft);          
        }

    }

    /**
     * Liefert die Zahl der Karotten die der Hase bei sich tr�gt.
     * 
     * @return Anzahl der Kartotten
     */
    public int getCarrotCount() {
        return carrotCount;

    }

    public final void act() {
        if (!initDone) {
            init();
            initDone = true;
        }
    }
    
    /**
     * Gibt den Text �ber dem Character aus.
     */
    public void say(String text){
        int y = getY() - 1;
        if (y < 0){
            y = 1;
        }
        getWorld().showText(text, getX(), y);
        System.out.println("a " + this.getClass().getName() + " says: " + text); 
        Greenfoot.delay(1);
    }
      
    public void say(int x){
        say(""+x);
    }

    
    public void say(boolean x){
        say(""+x);
    }
    
    public void say(double x){
        say(""+x);
    }


    /**
     * Die init() Methode, sie wird einmalig beim Programmstart aufgerufen
     */
    public abstract void init();

}